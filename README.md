# FFuzz
### Fuzzy logic library in F#
This is the backend library for the Grasshopper plug-in Angora 2.

I started this project to learn F# and it was so much fun so I decided to make a new version for Angora wich uused the [AForge.NET](http://aforgenet.com/framework/) library.

Parser combinators ([FParsec](http://www.quanttec.com/fparsec/)) are the bomb!
