﻿module Sailient.FFuzz.FCLParsing

open System
open FParsec

open Sailient.FFuzz
open Sailient.FFuzz.ParsingPrimitives
open Sailient.FFuzz.Primitives
open Sailient.FFuzz.Types
open Sailient.FFuzz.MethodsConfig
open Sailient.FFuzz.InferenceSystem


// ====================================================================================================================

let pMLComment :Parser<_,unit> =
     anyStringBetween (pstring "(*") (pstring "*)")
     <?> "Multiline comment"

let pComment :Parser<_,unit> =
     skipBlank
     >>. (skipString "//")
     >>. skipBlank
     >>. restOfLine false
     <?> "Comment"

let pCommentBeforeEOL :Parser<_,unit> = opt pComment

// ====================================================================================================================


let pIdentifier :Parser<_,unit> =
    asciiUpper .>>. many1 (choice [asciiLetter ; digit ; pchar '_' ])
    |>> fun (a, b) -> (string a) + String(List.toArray b)


let pIndent =
     newline >>? (skipString "  " >>. skipMany pBlank) <??> "Indentation"


let pProp n c p =
    pIndent
    >>? pstring n
    >>. skip1Blank
    >>. skipString c
    >>. skip1Blank
    >>. p
    .>> skipBlank
    .>> pCommentBeforeEOL

let pNamedProp n c p =
    pIndent
    >>? pstring n
    >>. skip1Blank
    >>. pIdentifier
    .>>. (skip1Blank
          >>. skipString c
          >>. skip1Blank
          >>. p )
    .>> skipBlank
    .>> pCommentBeforeEOL

// ====================================================================================================================


let pRangeValue :Parser<_,unit> =
    pfloat .>>. (skip1Blank >>. skipString ".." >>. skip1Blank >>. pfloat)

let pRange = 
    (pProp "RANGE" "=" pRangeValue)
     <?> "Range definition"


// ============================================ //
//       TERM


/// Parse a FuzzyTerm,
/// i.e.   "TERM Near_Death = (0, 0) (0, 1) (50, 0)"
/// or     "TERM Run_Away = 1"
let pTerm =

    // values
    let pPoint :Parser<float*float, unit>=
        (optional (pchar ' ')) >>. (str_s "(" >>. pfloat .>> str_s ",") .>>. pfloat .>> str_s ")"
         <??> "Point"

    let pSingl :Parser<float, unit> =
        (optional (pchar ' ')) >>. pfloat
        <??> "Float"

    let pTxx =
        (skipBlank >>. str_s1 "T") >>? sepBy1 pfloat skip1Blank <??> "T set"

    let pTuoleXX side =
        (skipBlank >>. str_s1 side) >>? (pfloat .>> skip1Blank .>>. pfloat)

    let pLft = pTuoleXX "L" <??> "Left edge set"
    let pRgt = pTuoleXX "R" <??> "Right edge set"
    let pBll = pTuoleXX "B" .>>.? (skip1Blank >>. pint32) <??> "Bell set"
    let pG = pTuoleXX "G" .>>.? (skip1Blank >>. pint32) <??> "Gaussian set"
    let pG2 = pTuoleXX "GW" .>>.? (skip1Blank >>. pint32) <??> "GaussianHW set"


    // sets
    let pPiecewise :Parser<FuzzyTerm, unit> =
        let p3Pts = (pPoint .>>. pPoint .>>. many1 pPoint) <??> "3 points minimum"
        pNamedProp "TERM" "=" p3Pts <??> "Piecewise term"
        |>> fun (n , ((xy0, xy1), xys)) ->
            let xs, ys = List.unzip (xy0::xy1::xys)
            match FuzzyTerm.createPiecewiseTerm n xs ys with
            | Result.Ok ft -> ft
            | Result.Error err -> failwith err
        

    let pSingleton :Parser<FuzzyTerm, unit> =
        pNamedProp "TERM" "=" pSingl <??> "Singleton term"
        |>> fun (n, x) ->
            match FuzzyTerm.createSingletonTerm n (x * 1.0<real>) with
            | Result.Ok ft -> ft
            | Result.Error err -> failwith err

    let pLeft :Parser<FuzzyTerm, unit> =
        pNamedProp "TERM" "=" pLft <??> "Left edge term"
        |>> fun (n, xs) ->
            match FuzzyTerm.createLowEdgeTerm n xs with
            | Result.Ok ft -> ft
            | Result.Error err -> failwith err

    let pRight :Parser<FuzzyTerm, unit> =
        pNamedProp "TERM" "=" pRgt <??> "Right edge term"
        |>> fun (n, xs) ->
            match FuzzyTerm.createHighEdgeTerm n xs with
            | Result.Ok ft -> ft
            | Result.Error err -> failwith err

    let pBell :Parser<FuzzyTerm, unit> =
        pNamedProp "TERM" "=" pBll <??> "Bell term"
        |>> fun (n, ((c, hw), s)) ->
            match FuzzyTerm.createBellTerm n c hw s with
            | Result.Ok ft -> ft
            | Result.Error err -> failwith err

    let pGaussian :Parser<FuzzyTerm, unit> =
        pNamedProp "TERM" "=" pG <??> "Gaussian term."
        |>> fun (n, ((sigma, c), s)) ->
            match FuzzyTerm.createGaussianTerm n sigma c s with
            | Result.Ok ft -> ft
            | Result.Error err -> failwith err

    let pGaussian2 :Parser<FuzzyTerm, unit> =
        pNamedProp "TERM" "=" pG2 <??> "Gaussian (halfWidth) term."
        |>> fun (n, ((c, hw), s)) ->
            match FuzzyTerm.createGaussianTermHalfWidth n c hw s with
            | Result.Ok ft -> ft
            | Result.Error err -> failwith err

    let pTSet :Parser<FuzzyTerm, unit> =
        pNamedProp "TERM" "=" pTxx <??> "Triangular/Trapezoidal term" 
        |>> fun (n , args) ->
            match args with
            | [ x1 ; x2 ; x3 ] ->
                match FuzzyTerm.createTriangularTerm n (x1, x2, x3) with
                | Result.Ok ft -> ft
                | Result.Error err -> failwith err   
            | [ x1 ; x2 ; x3 ; x4] ->
                match FuzzyTerm.createTrapezoidalTerm n (x1, x2, x3, x4) with
                | Result.Ok ft -> ft
                | Result.Error err -> failwith err  
            | _ -> failwithf "Term '%s' : invalid argument count."  n

    //(attempt pPiecewise) <|> pSingleton <??> "TERM"
    choice
        [ attempt pLeft 
          attempt pRight 
          attempt pTSet 
          attempt pGaussian 
          attempt pGaussian2
          attempt pBell 
          attempt pPiecewise 
          pSingleton]
    <?> "Fuzzy term"

let pDefuzz :Parser<_,unit> =
     (pProp "DEFUZZ" ":" OperatorParsing.pDefuzz)
     <?> "Defuzzifier method"


// ==============================
let pInputBlockBegin =
    pstring "INPUT" >>. skipMany1 pBlank >>. pIdentifier .>> pCommentBeforeEOL

let pInputBlock =
    pInputBlockBegin
    .>>. opt pRange
    .>>. many1 pTerm
    .>> newline .>> spaces
    |>> fun ((n , ro), trms) ->
            let vb = FuzzyVarBuilder(n)
            match ro with
            | Some (a,b) -> do vb.SetBounds (a * 1.0<real>,b * 1.0<real>)
            | _ -> ()
            do vb.AddTerms trms
            vb.CreateInputVar()

// ==============================
let pOutputBlockBegin =
    pstring "OUTPUT" >>. skipMany1 pBlank >>. pIdentifier .>> pCommentBeforeEOL

let pOutputBlock =
    pOutputBlockBegin
    .>>. opt pRange
    .>>. many1 pTerm
    .>>. pDefuzz
    .>> newline .>> spaces
    |>> fun (((n , ro), trms), d) ->
        let vb = FuzzyVarBuilder(n)
        match ro with
        | Some (a,b) -> do vb.SetBounds (a * 1.0<real>,b * 1.0<real>)
        | _ -> ()
        do vb.AddTerms trms
        vb.CreateOutputVar(d)


// ============================================ //
//      METHODS


let pOr   = pProp "OR" ":" OperatorParsing.pUnion <?> "OR operator method"
let pAnd  = pProp "AND" ":" OperatorParsing.pIntersection <?> "AND operator method"
let pNot  = pProp "NOT" ":" OperatorParsing.pComplement <?> "NOT operator method"
let pAcc  = pProp "ACCU" ":" OperatorParsing.pAccum <?> "ACCU method"

let pCustomOr  (methods :MethodsConfig) = pProp "OR" ":" (OperatorParsing.pBinaryOpList [| methods.UnionOperator |]) <?> "OR operator method"
let pCustomAnd (methods :MethodsConfig) = pProp "AND" ":" (OperatorParsing.pBinaryOpList [| methods.IntersectionOperator |]) <?> "AND operator method"
let pCustomNot (methods :MethodsConfig) = pProp "NOT" ":" (OperatorParsing.pUnaryOpList [| methods.ComplementOperator |]) <?> "NOT operator method"


let pSamp  :Parser<int,unit> =
     pProp "SAMPLING" "=" pint32 <?> "SAMPLING"

let pMethods = 
    str_s "METHODS" >>. skipRestOfLine false
    >>. pOr .>>. pAnd .>>. pNot .>>. pAcc .>>. opt pSamp
    |>> fun ((((or', and'), not'), acc'), so) ->
            let mc = { MethodsConfig.Default with
                          UnionOperator = or'
                          IntersectionOperator = and'
                          ComplementOperator = not'
                          Accumulator = acc' }
            match so with
            | None -> mc
            | Some s -> { mc with Sampling = s }



let pCustomMethods methods = 
    str_s "METHODS" >>. skipRestOfLine false
    >>. pCustomOr methods .>>. pCustomAnd methods .>>. pCustomNot methods .>>. pAcc .>>. opt pSamp
    |>> fun ((((or', and'), not'), acc'), so) ->
            let mc = { MethodsConfig.Default with
                          UnionOperator = or'
                          IntersectionOperator = and'
                          ComplementOperator = not'
                          Accumulator = acc' }
            match so with
            | None -> mc
            | Some s -> { mc with Sampling = s }

let pAFLcustom methods =
    (pstring "#ANGORA_2" >>. skipBlank >>. newline)
    >>. (opt pMLComment .>> spaces)
    >>. many1 (pInputBlock .>> spaces)
    .>>. many1 (pOutputBlock .>> spaces)
    .>>. (pCustomMethods methods .>> spaces)
    .>>. anyStringBefore eof
    |>> fun (((vi, vo), m), rs) ->
            let infSysBuilder = InferenceSystemBuilder(methods)
            let iv_errors = infSysBuilder.TryAddInputVariables(vi)
            let ov_errors = infSysBuilder.TryAddOutputVariables(vo)
            let rules = rs.Split([| ';' |], StringSplitOptions.RemoveEmptyEntries)
            let r_errors = infSysBuilder.TryAddRules(rules)
            match infSysBuilder.TryCreateInferenceSystem(false) with
            | Result.Ok infSysB -> Result.Ok infSysB
            | Result.Error err -> Result.Error <| err :: iv_errors @ ov_errors @ r_errors


let pAFL =
    (pstring "#ANGORA_2" >>. skipBlank >>. newline)
    >>. (opt pMLComment .>> spaces)
    >>. many1 (pInputBlock .>> spaces)
    .>>. many1 (pOutputBlock .>> spaces)
    .>>. (pMethods .>> spaces)
    .>>. anyStringBefore eof
    |>> fun (((vi, vo), m), rs) ->
            let infSysBuilder = InferenceSystemBuilder(m)
            let iv_errors = infSysBuilder.TryAddInputVariables(vi)
            let ov_errors = infSysBuilder.TryAddOutputVariables(vo)
            let rules = rs.Split([| ';' |], StringSplitOptions.RemoveEmptyEntries)
            let r_errors = infSysBuilder.TryAddRules(rules)
            match infSysBuilder.TryCreateInferenceSystem(false) with
            | Result.Ok infSysB -> Result.Ok infSysB
            | Result.Error err -> Result.Error <| err :: iv_errors @ ov_errors @ r_errors
// ============================================ //
//      Parser classes



type InputVarParser() =
    static member Parse(str) =
        match run pInputBlock str with
        | Success (v,_,_) -> Result.Ok v
        | Failure (err,_,_)-> Result.Error err


type OutputVarParser() =
    static member Parse(str) =
        match run pOutputBlock str with
        | Success (v,_,_) -> Result.Ok v
        | Failure (err,_,_)->  Result.Error err


type MethodsParser() =
    static member Parse(str) =
        match run pMethods str with
        | Success (v,_,_) -> Result.Ok v
        | Failure (err,_,_)->  Result.Error err



type AFLParser =

    static member isBlankOrComment (line :string) =
        (String.IsNullOrWhiteSpace (line.TrimEnd()) || line.Trim().StartsWith("//"))

    static member Parse (file :string) =
        file.Split([| '\n' |])
        |> Array.filter (fun l -> not (AFLParser.isBlankOrComment l))
        |> fun arr -> String.Join ("\n", arr)
        |> run pAFL
        |> function
           | Success (x,_,_) -> x
           | Failure (err,_,_) -> failwith err

    static member Parse (file :string, methods :MethodsConfig) =
        file.Split([| '\n' |])
        |> Array.filter (fun l -> not (AFLParser.isBlankOrComment l))
        |> fun arr -> String.Join ("\n", arr)
        |> run (pAFLcustom methods)
        |> function
           | Success (x,_,_) -> x
           | Failure (err,_,_) -> failwith err