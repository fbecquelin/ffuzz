﻿module Sailient.FFuzz.Types

open System
open Sailient.FFuzz.Primitives
open Sailient.FFuzz.SetOperators
open Operators


/// Linguistic Term
type FuzzyTerm =
    { Name :string
      Var :string
      Term :string
      FuzzySet :FuzzySet }

    override __.ToString() =
        let kind fs =
            match fs with
            | PiecewiseSet pws when pws.Edge = Edge.No ->
                let a, b = (PiecewiseSet.bounds pws)
                sprintf "PiecewiseSet [ %g ; %g ]" a b
            | PiecewiseSet pws when pws.Edge = Edge.Left ->
                let _, b = (PiecewiseSet.bounds pws)
                sprintf "PiecewiseSet [ _ ; %g ]" b
            | PiecewiseSet pws when pws.Edge = Edge.Right ->
                let a, b = (PiecewiseSet.bounds pws)
                sprintf "PiecewiseSet [ %g ; _ ]" a
            | PiecewiseSet pws when pws.Edge = Edge.Both ->
                sprintf "PiecewiseSet [ _ ; _ ]"
            | Singleton s -> sprintf "Singleton [ %g ]" s.Point.X
            | _ -> failwithf "What kind of FuzzySet is this?? \n %A" fs

        sprintf "FFuzz.FuzzyTerm \"%s\" of %s" __.Name (kind __.FuzzySet)


module FuzzyTerm =

    /// Sort terms by their typical value.
    let sort = Array.sortBy (fun ft -> FuzzySet.getMaximum ft.FuzzySet)

    let isSingleton ft =
        match ft.FuzzySet with
        | Singleton s -> true
        | PiecewiseSet p -> false

    let bounds ft =
        FuzzySet.bounds ft.FuzzySet


    let createSingletonTerm name x =
        match Identifier.create name with
        | Ok ident -> Ok {Name = ident.Value ; Var = "" ; Term = ident.Value ; FuzzySet = Singleton (Singleton.create x) }
        | Error err -> Error <| sprintf "Term '%s': %s" name err

    let createTrapezoidalTerm name (x1, x2, x3, x4) =
        if (x1 <= x2) && (x2 < x3) && (x3 <= x4) then
            let points = [| FuzzyPoint(x1 * 1.0<real>, 0.0<truth>)
                            FuzzyPoint(x2 * 1.0<real>, 1.0<truth>)
                            FuzzyPoint(x3 * 1.0<real>, 1.0<truth>)
                            FuzzyPoint(x4 * 1.0<real>, 0.0<truth>) |]
            match Identifier.create name with
            | Ok ident -> Ok {Name = ident.Value ; Var = "" ; Term = ident.Value ; FuzzySet = PiecewiseSet (PiecewiseSet.createSet points) }
            | Error err -> Error <| sprintf "Term '%s': %s" name err
        else
            Error "Values are not sorted."

    let createTriangularTerm name (x1, x2, x3) =
        if (x1 < x2) && (x2 < x3) then
            let points = [| FuzzyPoint(x1 * 1.0<real>, 0.0<truth>)
                            FuzzyPoint(x2 * 1.0<real>, 1.0<truth>)
                            FuzzyPoint(x3 * 1.0<real>, 0.0<truth>) |]
            match Identifier.create name with
            | Ok ident -> Ok {Name = ident.Value ; Var = "" ; Term = ident.Value ; FuzzySet = PiecewiseSet (PiecewiseSet.createSet points)}
            | Error err -> Error <| sprintf "Term '%s': %s" name err
        else
            Error "Values are not sorted."

    let createLowEdgeTerm name (x1, x2) =
            match Identifier.create name with
            | Ok ident ->
                let sR = PiecewiseSet.createLeftEdge x1 x2
                match sR with
                | Ok s -> Ok {Name = ident.Value ; Var = "" ; Term = ident.Value ; FuzzySet = PiecewiseSet s }
                | Error err -> Error err
            | Error err -> 
                Error <| sprintf "Term '%s': %s" name err


    let createHighEdgeTerm name (x1, x2) =
            match Identifier.create name with
            | Ok ident -> 
                let sR = PiecewiseSet.createRightEdge x1 x2
                match sR with
                | Ok s -> Ok {Name = ident.Value ; Var = "" ; Term = ident.Value ; FuzzySet = PiecewiseSet s }
                | Error err -> Error err
            | Error err -> Error <| sprintf "Term '%s': %s" name err


    let createPiecewiseTerm name (xs :float seq) (ys :float seq) =
        let distinctXs = Seq.distinct xs
        let len = Seq.length distinctXs
        if len = 0 then
            Error <| sprintf "Term '%s': No data." name
        elif len <> Seq.length xs then
            Error <| sprintf "Term '%s': Duplicate x values." name
        elif len <> Seq.length ys then
            Error <| sprintf "Term '%s': Xs and Ys lists have different length." name
        else
            let pointsResult = Seq.zip distinctXs ys
                               |> Seq.sortBy (fun (a, _) -> a) 
                               |> Seq.map (fun (x', y') -> FuzzyPoint.create x' y')
                               |> Seq.toList
                               |> Result.sequence
            match pointsResult with
            | Error err -> Error <| sprintf "Term '%s': %s" name err
            | Ok points -> 
                    match Identifier.create name with
                    | Ok ident -> Ok {Name = ident.Value ; Var = "" ; Term = ident.Value ; FuzzySet = PiecewiseSet (PiecewiseSet.createSet (List.toArray points)) }
                    | Error err -> Error <| sprintf "Term '%s': %s" name err


    let private range (a: float) (b: float) n =
            let l = b - a
            let r = [| for i in 0..n -> (a + (float i) * l / (float n)) |]
            r


    let createGaussianTerm name (sigma: float) (center: float) (sampling :int) =
        let mu s c x =
                exp ( ( -(x - c)*(x - c)) / (2.0 * s*s) )

        if sampling <= 0 then
                Error <| sprintf "Term '%s': Sampling must be positive (value: %i)" name sampling
        else
            if sigma <= 0.0 then
                Error <| sprintf "Term '%s': Sigma must be positive (value: %g)" name sigma
            else
                let xs = range (center - 4.0 * sigma) (center +  4.0 * sigma) (2 * sampling)
                let ys = Array.map (fun x -> mu sigma center x) xs
                ignore <| Array.set ys 0 0.0
                ignore <| Array.set ys (ys.Length - 1) 0.0
                createPiecewiseTerm name xs ys

    let createGaussianTermHalfWidth name (center: float) (hw: float)  (sampling :int) =
        let ratio = 1.0 / sqrt (8.0 * log 2.0)
        createGaussianTerm name (hw * ratio) center sampling

    let createBellTerm name (center: float) (halfWidth: float) (sampling :int) =

        let subMu (a: float) (b: float) (c: float) (x: float) =
            if x < a then
                 0.0
            else if (a <= x) && (x < b) then
                ( 2.0 * (x - a)**2.0 ) / ( (c - a)**2.0 )
            else if (b <= x) && (x <= c)then
                 1.0 - ( 2.0 * (x - c)**2.0 ) / ( (c - a)**2.0 )
            else // x > c
                1.0

        let mu (hwdth: float) (cntr: float) x =
            if x <= cntr then
                let y = subMu (cntr - hwdth) (cntr - 0.5*hwdth) (cntr) x
                y
            else // x > cntr
                let y = 1.0 - ( subMu (cntr) (cntr + 0.5*hwdth) (cntr + hwdth) x )
                y

        if sampling <= 0 then
            Error <| sprintf "Term '%s': Sampling must be positive (value: %i)" name sampling
        else
            if halfWidth <= 0.0 then
                Error <| sprintf "Term '%s': HalfWidth must be positive (value: %g)" name halfWidth
            else
                let xs = range (center - halfWidth) (center + halfWidth) (2 * sampling)
                let ys = Array.map (fun x -> mu halfWidth center x) xs
                createPiecewiseTerm name xs ys


    let eval (x :float<real>) (ft :FuzzyTerm) =
        match ft.FuzzySet with
        | Singleton s -> 
            if x = s.Point.X then 1.0<truth> else 0.0<truth> 
        | PiecewiseSet p -> 
            PiecewiseSet.eval p x

    let maxPoint (ft :FuzzyTerm) =
        FuzzySet.getMaximum ft.FuzzySet

    let getFiringStrength (world :Map<string,float<real>>) (ft :FuzzyTerm) =
        if Map.containsKey ft.Var world then
            let input = world.[ft.Var]
            eval input ft
        else
            failwithf "Variable \"%s\" was not found in inputs." ft.Var

    let truncateSet (ft :FuzzyTerm) (s :float<truth>) =
        FuzzySet.truncate ft.FuzzySet s

    let truncate ft a =
        { ft with FuzzySet = truncateSet ft a }

    let transformTerm (ft :FuzzyTerm) (m :UnarySetOperator) =
        match ft.FuzzySet with
        | Singleton s -> failwithf "Cannnot apply modifier %s to singleton term %s." m.Name ft.Name
        | PiecewiseSet fs ->
            let newFs = m.Func (PiecewiseSet fs)
            let newName = sprintf "%s(%s)" m.Name ft.Name
            { ft with Name = newName ; FuzzySet = newFs }


type FuzzyTerm with

     member __.Bounds = FuzzyTerm.bounds __

     member __.GetMaxPoint() =
        FuzzyTerm.maxPoint __ 

     member __.Truncate(s) =
        FuzzyTerm.truncate __ s


     static member CreateSingletonTerm(name, x) =
        FuzzyTerm.createSingletonTerm name x

     static member CreateTrapezoidalTerm(name, x1, x2, x3, x4) =
        FuzzyTerm.createTrapezoidalTerm name (x1, x2, x3, x4)

     static member CreateTriangularTerm(name, x1, x2, x3) =
        FuzzyTerm.createTriangularTerm name (x1, x2, x3)

     static member CreateLowEdgeTerm(name, x1, x2) =
        FuzzyTerm.createLowEdgeTerm name (x1, x2)

     static member CreateHighEdgeTerm(name, x1, x2) =
        FuzzyTerm.createHighEdgeTerm name (x1, x2)

     static member CreatePiecewiseTerm (name, (xs :float seq), (ys :float seq)) =
        FuzzyTerm.createPiecewiseTerm name xs ys

     static member CreateGaussianTerm(name, sigma, center, sampling) =
        FuzzyTerm.createGaussianTerm name sigma center sampling

     static member CreateGaussianTermHalfWidth (name, center, halfWidth, sampling) =
        FuzzyTerm.createGaussianTermHalfWidth name center halfWidth sampling

     static member CreateBellTerm(name, center, halfWidth, sampling) =
        FuzzyTerm.createBellTerm name center halfWidth sampling





/// Input Linguistic Variable.
type FuzzyInputVar =
    internal { name :string
               range :float<real> * float<real>
               fuzzyTerms :FuzzyTerm array }

    member __.Name = __.name
    member __.Range = __.range
    member __.Terms = __.fuzzyTerms

    member __.TermsNames = __.fuzzyTerms 
                           |> Array.map (fun t -> t.Name)

    member __.TermsSets = __.fuzzyTerms 
                          |> Array.map (fun t -> t.FuzzySet)

    override __.ToString() =
        let minV,maxV = __.range
        sprintf "FFuzz.FuzzyInputVar \"%s\" of (%s) [ %g ; %g ]" __.Name (String.Join(", ", __.TermsNames)) minV maxV


module FuzzyInputVar =
    
    let eval (fv :FuzzyInputVar) x =
        fv.Terms
        |> Array.map (FuzzyTerm.eval x)


type FuzzyInputVar with

     member __.Eval(x) =
        FuzzyInputVar.eval __ x


/// Output Linguistic Variable.
type FuzzyOutputVar =
    internal { name :string
               range :float<real> * float<real>
               fuzzyTerms :FuzzyTerm array 
               defuzzifier :Defuzzifier }

    member __.Name = __.name
    member __.Range = __.range
    member __.Terms = __.fuzzyTerms

    member __.TermsNames = __.fuzzyTerms 
                           |> Array.map (fun t -> t.Name)

    member __.TermsSets = __.fuzzyTerms 
                          |> Array.map (fun t -> t.FuzzySet)

    member __.Defuzzifier = __.defuzzifier

    override __.ToString() =
        let minV,maxV = __.range
        sprintf "FFuzz.FuzzyOutputVar \"%s\" of (%s) [ %g ; %g ] (%s)"
            __.Name
            (String.Join(", ", __.TermsNames))
            minV
            maxV
            __.Defuzzifier.Name

module FuzzyOutputVar =
    
    let toInputVar (fv :FuzzyOutputVar) =
        { FuzzyInputVar.name = fv.Name
          range = fv.Range
          fuzzyTerms = fv.Terms }

    let eval (fv :FuzzyOutputVar) x =
        fv.Terms
        |> Array.map (FuzzyTerm.eval x)


type FuzzyOutputVar with

     member __.ToInputVar() =
        FuzzyOutputVar.toInputVar __

     member __.Eval(x) =
        FuzzyOutputVar.eval __ x

/// <summary>
/// Builder class for linguistic variables.
/// </summary>
/// <exception cref="System.Exception>Thrown when the name is not a valid identifier.</exception>
/// <example>
/// <code>
/// var fvb = FuzzyVarBuilder("Temperature");
/// fvb.SetBounds(-10, 50); // optional
/// fvb.AddTerms(terms);
/// var myFuzzyVar = fvb.CreateVar();
/// </code>
/// </example>
type FuzzyVarBuilder(name :string) =
    class

        let mutable m_name = name;
        let mutable m_min = Double.PositiveInfinity * 1.0<real>
        let mutable m_max = Double.NegativeInfinity * 1.0<real>
        let mutable m_autoBounds = true
        let mutable m_terms :FuzzyTerm list = []

        let termNames terms = List.map (fun t -> t.Name) terms

        let includesTerm (minX, maxX) term =
            let a, b = FuzzySet.bounds term.FuzzySet
            not ( a < minX || b > maxX )

        let checkTermsAgainstBounds (minX, maxX) =
            if List.length m_terms = 0 then
                true
            else
                m_terms
                |> List.map (includesTerm (minX, maxX))
                |> List.reduce (&&)


        let addTerm term =
            if List.contains term.Name (termNames m_terms) then
               failwithf "Duplicate term name : %s" term.Name
            elif not (includesTerm (m_min, m_max) term) then
               failwithf "Term out of range : %s" term.Name
            else
                m_terms <- term::m_terms

        let addTermForce term =
            if List.contains term.Name (termNames m_terms) then
               failwithf "Duplicate term name : %s" term.Name
            else
                let a, b = FuzzySet.bounds term.FuzzySet
                m_min <- min a m_min
                m_max <- max b m_max
                m_terms <- term::m_terms

        let configTerm term =
            match term.FuzzySet with
            | Singleton ss ->
                { term with Var = m_name }
            | PiecewiseSet pws when pws.Edge = Edge.No ->
                { term with Var = m_name }
            | PiecewiseSet pws ->
                match PiecewiseSet.extend pws (m_min, m_max) with
                | Ok expanded -> { term with Var = m_name ; FuzzySet = PiecewiseSet expanded }
                | Error err -> failwith err


        do
            match Identifier.create name with
            | Ok ident ->
                m_name <- ident.Value
            | Error err -> failwith err


        member __.VarName = m_name

        /// <summary>
        /// Specify the variable range
        /// </summary>
        /// <param name="a">Lowest value</param>
        /// <param name="b">Highest value</param>
        /// <exception cref="System.Exception>Thrown when already added terms are out of the new interval.
        /// You should set the optional bounds right after construction anyway.</exception>
        member __.SetBounds(a, b) =
            let minX , maxX = min a b, max a b
            if checkTermsAgainstBounds (minX, maxX) then
                m_min <- minX
                m_max <- maxX
                m_autoBounds <- false
            else
                failwith "Term out of range on new variable bounds."

        /// <summary>
        /// Add a term.
        /// </summary>
        /// <param name="term">The term to add.</param>
        /// <exception cref="System.Exception>Thrown when the name is already present 
        /// or the term is out of the specified bounds.</exception>
        member __.AddTerm(term) =
            if m_autoBounds then
                addTermForce term
            else 
                addTerm term

        /// <summary>
        /// Add several terms.
        /// </summary>
        /// <param name="term">The terms to add.</param>
        /// <exception cref="System.Exception>Thrown when the name is already present 
        /// or the term is out of the specified bounds.</exception>
        member __.AddTerms(terms) =
            if m_autoBounds then
                Seq.iter addTermForce terms
            else 
                Seq.iter addTerm terms

        /// <summary>
        /// Create a FuzzyInputVar instance.
        /// </summary>
        /// <returns>The linguistic variable.</returns>
        member __.CreateInputVar() = 
            let terms = m_terms 
                        |> List.map configTerm
                        |> List.sortBy(fun t -> (FuzzySet.getMaximum t.FuzzySet).X)
            { FuzzyInputVar.name = m_name
              range = m_min, m_max
              fuzzyTerms = List.toArray terms }


        /// <summary>
        /// Create a FuzzyOutputVar instance.
        /// </summary>
        /// <returns>The linguistic variable.</returns>
        member __.CreateOutputVar(f :Defuzzifier) = 
            let terms = m_terms 
                        |> List.map configTerm
                        |> List.sortBy(fun t -> (FuzzySet.getMaximum t.FuzzySet).X)
            let is_not_cogs = f.Name <> Defuzzifier.CoGS.Name
            let contains_singletons = List.exists FuzzyTerm.isSingleton terms
            if is_not_cogs && contains_singletons then
                failwithf "You must use the CoGS defuzzifier with variables that contain singleton terms (%s)." m_name
            else
                { FuzzyOutputVar.name = m_name
                  range = m_min, m_max
                  fuzzyTerms = List.toArray terms 
                  defuzzifier = f }

    end
