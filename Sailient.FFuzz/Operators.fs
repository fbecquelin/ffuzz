﻿module Sailient.FFuzz.Operators

open System
open Sailient.FFuzz.Primitives

// Operators that act on numbers

// See:
//     http://rorchard.github.io/FuzzyJ/fuzzyJDocs/FuzzyModifier.html

type UnaryOperator =
       { Name :string
         Func :float<truth> -> float<truth> }

        override __.ToString() =
             sprintf "FFuzz.UnaryOperator %s" __.Name

module UnaryOperator =

    let call op (x :float<truth>) = 
        op.Func x
       
    let apply op (fp :FuzzyPoint) = 
        FuzzyPoint(fp.X, op.Func fp.Y)

    let compose (a :UnaryOperator) (b :UnaryOperator) =
        { Name = sprintf "%s_%s" b.Name a.Name  // PN
          Func = a.Func >> b.Func }

    let createCustom (name :string) (func :Func<float,float>) :UnaryOperator =
        match Identifier.create name with
        | Ok ident ->
            let unaryLambda = fun a -> func.Invoke(a / 1.0<truth>) * 1.0<truth>
            { UnaryOperator.Name = ident.Value; Func = unaryLambda }
        | Error err -> failwith err


type UnaryOperator with

    member __.Call(x) =
        UnaryOperator.call __ x


    /// <summary>Create a custom unary operator with a delegate.</summary>
    /// <param name="name">The name of the operator.</param>
    /// <param name="func">The delegate function of type <c>Func<double,double></c>.</param>
    /// <returns>The unary operator.</returns>
    /// <example>
    /// <code>
    /// Func<double,double> opFunc =
    ///    (x =>
    ///    { // Your code here…
    ///        if (x > 0.6) 0 else 1;
    ///    });
    /// var myOp = FFuzz.SetOperators.UnaryOperator.CreateCustomUnaryOperator("ThresholdComplement", opFunc);
    /// </code>
    /// </example>
    static member CreateCustomUnaryOperator(name, func) =
        UnaryOperator.createCustom name func

    // Complements (c-norms)

    static member OpStdNeg =
        { Name = "StandardNeg"
          Func = fun x -> 1.0<truth> - x }

    static member OpCosineNeg =
        { Name = "CosineNeg"
          Func = fun x -> 0.5 * (1.0 + cos (Math.PI * x / 1.0<truth>)) * 1.0<truth> }

    static member OpSugeno lambda =
        let l = abs lambda
        { Name = sprintf "Sugeno:%g" lambda
          Func = fun x -> (1.0<truth> - x) / float (1.0<truth> - l * x) }

    static member OpYager mu =
        let l = abs mu
        { Name = sprintf "Yager:%g" mu
          Func = fun x -> (1.0 - float x ** l) ** ( 1.0 / l) * 1.0<truth>}

    static member DefaultComplements =
        [|
            UnaryOperator.OpStdNeg
            UnaryOperator.OpCosineNeg

            UnaryOperator.OpSugeno 12.0
            UnaryOperator.OpSugeno  2.0
            UnaryOperator.OpSugeno  0.0
            UnaryOperator.OpSugeno -0.7
            UnaryOperator.OpSugeno -0.95

            UnaryOperator.OpYager 0.5
            UnaryOperator.OpYager 0.7
            UnaryOperator.OpYager 1.0
            UnaryOperator.OpYager 1.5
            UnaryOperator.OpYager 3.0
        |]


    // Operators | Modifiers
    
    static member OpIntensify =
        { Name = "INTENSIFY"
          Func = fun x ->
                    let v = x / 1.0<truth>
                    if x <= 0.5<truth> then
                        (2.0 * v ** 2.0) * 1.0<truth>
                    else
                        (1.0 - 2.0 * (1.0 - v) ** 2.0) * 1.0<truth>
        }

    // Modifiers

    static member OpAny = 
        { Name = "ANY"
          Func = fun x -> if x = 0.0<truth> then 0.0<truth> else 1.0<truth> }

    static member OpVaguely = 
        { Name = "VAGUELY"
          Func = fun x -> (x / 1.0<truth>) ** 0.5 * 1.0<truth> }

    static member OpSomewhat = 
        { Name = "SOMEWHAT"
          Func = fun x -> (x / 1.0<truth>) ** 0.6666667 * 1.0<truth> }

    static member OpRather = 
        { Name = "RATHER"
          Func = fun x -> (x / 1.0<truth>) ** 1.5 * 1.0<truth> }

    static member OpVery =
        { Name = "VERY"
          Func = fun x -> (x / 1.0<truth>) ** 2.0 * 1.0<truth> }

    //static member OpExtremely =
    //    { Name = "EXTREMELY"
    //      Func = fun x -> (x / 1.0<truth>) ** 3.0 * 1.0<truth> }

    static member OpPure = 
        { Name = "PURE"
          Func = fun x -> if x = 1.0<truth> then 1.0<truth> else 0.0<truth> }

    static member DefaultModifiers =
        [|
           UnaryOperator.OpAny
           UnaryOperator.OpVaguely
           UnaryOperator.OpSomewhat
           UnaryOperator.OpRather
           UnaryOperator.OpVery
           UnaryOperator.OpPure
        |]
                      
                     


type BinaryOperator =
       { Name :string
         Func :float<truth> * float<truth> -> float<truth> }

        override __.ToString() =
             sprintf "FFuzz.BinaryOperator %s" __.Name


module BinaryOperator =
        let call (op :BinaryOperator) a b = 
            op.Func(a, b)

        let createCustom (name :string) (func :Func<float,float,float>) :BinaryOperator =
            match Identifier.create name with
            | Ok ident ->
                let binaryLambda = fun (a, b) -> func.Invoke(a / 1.0<truth>, b / 1.0<truth>) * 1.0<truth>
                { BinaryOperator.Name = ident.Value ; Func = binaryLambda }
            | Error err -> failwith err



type BinaryOperator with
    
    member __.Call(x, y) =
        BinaryOperator.call __ x y

    /// <summary>Create a custom binary operator with a delegate.</summary>
    /// <param name="name">The name of the operator.</param>
    /// <param name="func">The delegate function of type <c>Func<double,double,double></c>.</param>
    /// <returns>The binary operator.</returns>
    static member CreateCustomBinaryOperator(name, func) =
        BinaryOperator.createCustom name func

    // Unions (t-conorms)

    static member OpMax = 
        { Name = "Max"
          Func = fun (a, b) ->  max a b }

    static member OpSumProduct = 
        { Name = "SumProduct"
          Func = fun (a, b) ->  a + b - a * b / 1.0<truth> }

    static member OpLukasiewiczSum = 
        { Name = "LukasiewiczSum"
          Func = fun (a, b) -> min 1.0<truth> (a + b) }

    static member OpModSum =
        { Name = "ModSum"
          Func = fun (a, b) -> match min a b with
                               | 0.0<truth> -> max a b
                               | _          -> 1.0<truth> }

    static member DefaultUnions =
        [|
           BinaryOperator.OpMax
           BinaryOperator.OpSumProduct
           BinaryOperator.OpLukasiewiczSum
           BinaryOperator.OpModSum 
        |]


    // Intersections (t-norms)

    static member OpMin = 
        { Name = "Min"
          Func = fun (a, b) ->  min a b }

    static member OpProduct = 
        { Name = "Product"
          Func = fun (a, b) ->  a * b / 1.0<truth> }

    static member OpLukasiewiczProd = 
        { Name = "LukasiewiczProduct"
          Func = fun (a, b) -> max 0.0<truth> (a + b - 1.0<truth>) }

    static member OpModProd =
        { Name = "ModProduct"
          Func = fun (a, b) -> match max a b with
                               | 1.0<truth> -> min a b
                               | _          -> 0.0<truth> }

    static member DefaultIntersections =
        [|
           BinaryOperator.OpMin
           BinaryOperator.OpProduct
           BinaryOperator.OpLukasiewiczProd
           BinaryOperator.OpModProd
        |]




/// Result of a rule's antecedent evaluation
type RuleFiringStrength =
    {
      RuleIndex  :int option
      Var       :string
      Term      :string
      Strength  :float<truth>
      Weight    :float        
    }


type RuleAccumulation =
    { Name :string
      Func :RuleFiringStrength array -> RuleFiringStrength }

    override __.ToString() =
        sprintf "FFuzz.RuleAccumulation %s" __.Name


module RuleAccumulation =

    let accumulateMax (termFirs :RuleFiringStrength array) =
        let fir = Array.maxBy (fun fir -> fir.Strength * fir.Weight) termFirs
        { fir with RuleIndex = None; Strength = fir.Strength * fir.Weight ; Weight = 1.0 }

    let accumulateMin (termFirs :RuleFiringStrength array) =
        let fir = Array.minBy (fun fir -> fir.Strength * fir.Weight) termFirs
        { fir with RuleIndex = None; Strength = fir.Strength * fir.Weight ; Weight = 1.0 }

    let accumulateAvg (termFirs :RuleFiringStrength array) =
        let folder (accS, accW) fir =
            (accS + fir.Strength * fir.Weight, accW + fir.Weight)
        let totS, totW = Array.fold folder (0.0<truth>, 0.0) termFirs
        let s = if totW = 0.0 then 0.0<truth> else totS / totW
        { termFirs.[0] with RuleIndex = None; Strength = s ; Weight = 1.0 }


type RuleAccumulation with
    static member Max = { RuleAccumulation.Name = "Maximum" ; Func = RuleAccumulation.accumulateMax }
    static member Avg = { RuleAccumulation.Name = "Average" ; Func = RuleAccumulation.accumulateAvg }
    static member Min = { RuleAccumulation.Name = "Minimum" ; Func = RuleAccumulation.accumulateMin }


    static member DefaultRuleAccumulations =
        [|
           RuleAccumulation.Max
           RuleAccumulation.Avg
           RuleAccumulation.Min
        |]



type Defuzzifier =
       { Name :string
         Func :(FuzzySet list -> float<real>) }

        override __.ToString() =
              sprintf "FFuzz.Defuzzifier %s" __.Name


module Defuzzifier =

    
    let private union = List.reduce FuzzySet.union

    /// Centroid methods

    //Center of gravity method
    let centerOfGravity (list :FuzzySet list) =
        match list with
            | [] -> 0.0<real>
            | [ h ] ->
                let props = FuzzySet.areaProps h
                props.Centroid.X
            | h::t ->
                let fs = union list
                let props = FuzzySet.areaProps fs 
                props.Centroid.X

    //Center of sum method
    let centerOfSums (list :FuzzySet list) =
            match list with
            | [] -> 0.0<real>
            | [ h ] ->
                let props = FuzzySet.areaProps h
                props.Centroid.X
            | h::t ->
                let folder (accA, accX) fs =
                    let props = FuzzySet.areaProps fs 
                    accA + props.Area, accX + props.Area * props.Centroid.X
                let init = (0.0, 0.0<real>) 
                let totA, totX = List.fold folder init list
                totX / totA

    //Center of max area method
    let centerOfMaxArea (list :FuzzySet list) =
        let props = list
                    |> List.map FuzzySet.areaProps
                    |> List.maxBy (fun p -> p.Area)
        props.Centroid.X

    let centerOfGravitySingletons (list :FuzzySet list) =
        let f (fs :FuzzySet) =
            match fs with
            | Singleton s -> 
                s.Point.X * s.Point.Y, s.Point.Y
            | PiecewiseSet pws ->
                let pt = FuzzySet.getMaximum fs
                pt.X  * pt.Y, pt.Y
        list
        |> List.map f
        |> List.unzip
        |> fun (p , e) -> (List.sum p) / (List.sum e)

    /// Maxima methods

    //Mean of maximums
    let meanOfMax fss = fss
                        |> union 
                        |> FuzzySet.getMaximum 
                        |> fun p -> p.X

    //First of maximums
    let leftMax fss = fss
                            |> union 
                            |> FuzzySet.getLeftMaximum 
                            |> fun p -> p.X

    //Last of maximums
    let rightMax fss = fss
                             |> union 
                             |> FuzzySet.getRightMaximum 
                             |> fun p -> p.X

type Defuzzifier with

    static member CoG = {Defuzzifier.Name = "CoG" ; Func = Defuzzifier.centerOfGravity }
    static member CoS = {Defuzzifier.Name = "CoS" ; Func = Defuzzifier.centerOfSums }
    static member CoA = {Defuzzifier.Name = "CoA" ; Func = Defuzzifier.centerOfMaxArea }

    static member CoGS = {Defuzzifier.Name = "CoGS" ; Func = Defuzzifier.centerOfGravitySingletons }

    static member MoM = {Defuzzifier.Name = "MoM" ; Func = Defuzzifier.meanOfMax }
    static member LM = {Defuzzifier.Name = "LM" ; Func = Defuzzifier.leftMax }
    static member RM = {Defuzzifier.Name = "RM" ; Func = Defuzzifier.rightMax }

    static member DefaultDefuzzifiers =
        [|
          Defuzzifier.CoG
          Defuzzifier.CoS
          Defuzzifier.CoA
          Defuzzifier.CoGS
          Defuzzifier.MoM
          Defuzzifier.LM
          Defuzzifier.RM 
        |]


