﻿module Sailient.FFuzz.InferenceSystem

open System
open System.Collections.Generic
open Sailient.FFuzz.Primitives
open Sailient.FFuzz.Types
open Sailient.FFuzz.Operators
open Sailient.FFuzz.MethodsConfig
open Sailient.FFuzz.Engine
open Sailient.FFuzz.RuleParsing


exception InvalidVariable of string
exception InvalidTerm of string
exception InvalidValue of string

type FuzzyInferenceReply =
    { Value :float<real>
      Failed :bool }


type InferenceSystem =
    private { outputVars    :FuzzyOutputVar list
              inputVarNames :string list
              rules         :RuleBase 
              accumulator   :RuleAccumulation }
              // defuzzifier   :Defuzzifier }

    member __.InputVarNames = List.toArray __.inputVarNames
    member __.OutputVars = List.toArray __.outputVars

    override __.ToString() =
        let inputs = List.reduce (fun a b -> a + ", " + b) __.inputVarNames
        let outputVarNames = List.map (fun (v :FuzzyOutputVar) -> v.Name) __.outputVars
        let outputs = List.reduce (fun a b -> a + ", " + b) outputVarNames
        sprintf "FFuzz.InferenceSystem of (%s) -> (%s)" inputs outputs


module InferenceSystem =

    let private findVar (is :InferenceSystem) (varName :string) =
        try
            List.find (fun (vr :FuzzyOutputVar) -> vr.Name = varName) is.outputVars
        with
        | _ -> raise(InvalidVariable <| sprintf "Variable \"%s\" was not found in the database." varName)


    let private findTerm (is :InferenceSystem) (varName :string) (termName :string) =
        try
            findVar is varName
            |> fun v -> v.Terms
            |> Array.find (fun trm -> trm.Name = termName)
        with
        | InvalidVariable(err) -> raise(InvalidVariable err)
        | _ -> raise(InvalidTerm <| sprintf "Term \"%s\" of variable \"%s\" was not found in the database (should never happen)." termName varName)


    let private failOnInfinityOrNaN x =
        if Double.IsInfinity(x / 1.0<real>) then raise(InvalidValue "Infinity")
        elif Double.IsNaN(x / 1.0<real>) then raise(InvalidValue "NaN")
        else { FuzzyInferenceReply.Value = x ; Failed = false }

    let private failOnAllZeros (firs :RuleFiringStrength[]) =
        if Array.forall (fun fir -> fir.Strength = 0.0<truth>) firs then raise(InvalidValue "None")
        else firs

    let private fuzzifyTerm (world :Map<string,float<real>>) (rulegroup : string * Rule array) =
        let _, rules = rulegroup
        Array.map (Rule.getFiringStrength world) rules
    
    let private sortbyVarOrder (is :InferenceSystem) (varName :string) (rulegroups : (string * Rule array) [])=
        let var = findVar is varName
        let names = var.TermsNames
        Array.collect (fun n -> Array.FindAll(rulegroups, fun (name,rules) -> name = n)) names
                 

    let fuzzifyVar (is :InferenceSystem) (world :Map<string,float<real>>) (var :string) =
        try
            let  (RuleBase rules) = is.rules
            rules
            |> Array.filter (fun r -> Rule.outputVar r = var)
            |> Array.groupBy (fun r -> Rule.outputTerm r)
            |> sortbyVarOrder is var
            |> Array.map (fuzzifyTerm world)
        with
        | InvalidVariable(err) -> failwith err
        | InvalidTerm(err) -> failwith err
        | :? System.ArgumentException -> raise(InvalidVariable <| sprintf "Variable \"%s\" was not found in the database." var)


    let private accumulateTerms (is :InferenceSystem) (firs :RuleFiringStrength[] array) =
        firs
        |> Array.map (is.accumulator.Func)

    let accumulateVar (is :InferenceSystem) (world :Map<string,float<real>>) (var :string) =
        fuzzifyVar is world var
        |> accumulateTerms is


    let defuzzifyVar (is :InferenceSystem) (var :string) (world :Map<string,float<real>>) =
        let defuzz = (findVar is var).defuzzifier
        accumulateVar is world var
        |> failOnAllZeros
        |> Array.map (fun fir -> FuzzyTerm.truncateSet (findTerm is fir.Var fir.Term) fir.Strength)
        |> Array.toList
        |> defuzz.Func
        |> failOnInfinityOrNaN


    let tryDefuzzifyVar (is :InferenceSystem) (var :string) (failSafeVal :float) (world :Map<string,float<real>>) =
        try
            defuzzifyVar is var world
        with
        | InvalidVariable(err) -> failwith err
        | InvalidTerm(err) -> failwith err
        | InvalidValue(_) -> { FuzzyInferenceReply.Value = failSafeVal * 1.0<real> ; Failed = true }
        | :? System.ArgumentException -> raise(InvalidVariable <| sprintf "Variable \"%s\" was not found in the database." var)


    let pprint is =
        let (RuleBase rs) = is.rules
        Array.map Rule.pprint rs

    let updateRuleWeights (is :InferenceSystem) (weights :float seq) (alwaysNormalize) =
        match RuleBase.updateWeights is.rules weights alwaysNormalize with
        | Error err -> Error err
        | Ok rb -> Ok { is with rules = rb }

    let updateRuleWeight (is :InferenceSystem) (index :int, weight :float) =
        match RuleBase.updateWeight is.rules (index, weight) with
        | Error err -> Error err
        | Ok rb -> Ok { is with rules = rb }      

    let updateRuleWeightsByIndex (is :InferenceSystem) (indices :int seq, weights :float seq)  =
        match RuleBase.updateWeightsByIndex is.rules (indices, weights) with
        | Error err -> Error err
        | Ok rb -> Ok { is with rules = rb }    
        
type InferenceSystem with

    /// <summary>
    /// Display the rules in reverse polish notation for troubleshooting.
    /// </summary>
    member __.Details = InferenceSystem.pprint __

    /// <summary>
    /// Modify the rule weights.
    /// </summary>
    member __.UpdateRuleWeights (weights, alwaysNormalize) = InferenceSystem.updateRuleWeights __ weights alwaysNormalize
    member __.UpdateRuleWeights (indices, weights) = InferenceSystem.updateRuleWeightsByIndex __ (indices, weights)
    
    member __.UpdateRuleWeight (index, weight) = InferenceSystem.updateRuleWeight __ (index, weight)


    /// <summary>
    /// Defuzzify an output variable and fallback to a failsafe value in case of an error.
    /// </summary>
    /// <param name="varName">The output variable to defuzzify.</param>
    /// <param name="failsafeVal">The value to return when the inference fails.</param>
    /// <param name="world">A dictionary of the input variable names and values.</param>
    /// <returns>The crisp output.</returns>
    member __.Defuzzify(varName, (failsafeVal :float), (world :IDictionary<string,float<real>>)) =
        let wrld = (world :> seq<_>)
                   |> Seq.map (|KeyValue|)
                   |> Map.ofSeq
        InferenceSystem.tryDefuzzifyVar __ varName failsafeVal wrld

    /// <summary>
    /// Fuzzify an output variable.
    /// </summary>
    /// <param name="varName">The output variable to fuzzify.</param>
    /// <param name="world">A dictionary of the input variable names and values.</param>
    /// <returns>The firing strength of each rule.</returns>
    member __.Fuzzify(varName, (world :IDictionary<string,float<real>>)) =
        let wrld = (world :> seq<_>)
                   |> Seq.map (|KeyValue|)
                   |> Map.ofSeq
        InferenceSystem.fuzzifyVar __ wrld varName

    /// <summary>
    /// Fuzzify an output variable and accumulate each term.
    /// </summary>
    /// <param name="varName">The output variable to fuzzify.</param>
    /// <param name="world">A dictionary of the input variable names and values.</param>
    /// <returns>The accumulated firing strength of each term.</returns>
    member __.Accumulate(varName, (world :IDictionary<string,float<real>>)) =
        let wrld = (world :> seq<_>)
                   |> Seq.map (|KeyValue|)
                   |> Map.ofSeq
        InferenceSystem.accumulateVar __ wrld varName




// Builder class
type InferenceSystemBuilder(config :MethodsConfig) =
    let m_config = config

    let mutable m_database = { Database.InputVars = [] ; OutputVars = [] }
    let mutable m_rulebase = RuleBase Array.empty
    let mutable m_parser = RuleParser(m_config, m_database)

    let updateDatabase newDB =
            m_database <- newDB
            m_parser <- RuleParser(m_config, m_database)

    let failOnError a =
        match a with
        | None -> ignore
        | Some err -> failwith err

    let failOnErrorList a =
        match a with
        | [] -> ignore
        | _  -> a
                |> List.reduce (fun a b -> a + "\n" + b)
                |> failwith


    // FACTORY

    let tryCreateInferenceSystem(alwaysNormalizeWeights) =
        if RuleBase.count m_rulebase > 0 then

            Result.Ok { InferenceSystem.outputVars = m_database.OutputVars
                        inputVarNames = List.map (fun (v :FuzzyInputVar) -> v.Name) m_database.InputVars
                        rules = RuleBase.normalizeWeights alwaysNormalizeWeights  m_rulebase
                        accumulator = m_config.Accumulator }
        else
            Result.Error "Rulebase is empty."


    // INPUT VARIABLES

    let tryAddInputVariable v =
        match Database.tryAddInputVar m_database v with
        | Ok newDb -> 
            updateDatabase newDb
            None
        | Error err -> 
            Some err

    let tryAddInputVariables vs =
         let folder (db, errs) v =
            match Database.tryAddInputVar db v with
            | Ok db' -> db', errs
            | Error err -> db, err::errs
         let newDb, errorList = Seq.fold folder (m_database, []) vs

         if errorList = [] then
            updateDatabase newDb
            []
         else
            errorList
            |> List.rev


    // OUTPUT VARIABLES

    let tryAddOutputVariable v =
        match Database.tryAddOutputVar m_database v with
        | Ok newDb -> 
            updateDatabase newDb
            None
        | Error err -> 
            Some err

    let tryAddOutputVariables vs =
         let folder (db, errs) v =
            match Database.tryAddOutputVar db v with
            | Ok db' -> db', errs
            | Error err -> db, err::errs
         let newDb, errorList = Seq.fold folder (m_database, []) vs

         if errorList = [] then
            updateDatabase newDb
            []
         else
            errorList
            |> List.rev


    // RULES

    let tryAddRule s =
        match m_parser.Parse (RuleBase.count m_rulebase, s) with
        | Ok rule ->
            m_rulebase <- RuleBase.addRule m_rulebase rule
            None
        | Error err ->
            Some (sprintf "Rule %i: %s" (RuleBase.count m_rulebase) err)


    let tryAddRules ss =
        let folder (i, rb, errs) s =
            match m_parser.Parse (RuleBase.count rb, s) with
            | Result.Ok rule ->
                i + 1, RuleBase.addRule rb rule, errs
            | Result.Error err -> i + 1, rb, (sprintf "Rule %i: %s" i err)::errs

        let init = (RuleBase.count m_rulebase, m_rulebase, [])
        let _, newRb, errorList = 
            ss
            |> Seq.filter (fun (s' :string) -> not <| String.IsNullOrWhiteSpace(s'))
            |> Seq.fold folder init 
        if errorList = [] then
            m_rulebase <- newRb
            []
        else
            errorList
            |> List.rev
            


    // MEMBERS
    
    member __.TryCreateInferenceSystem(alwaysNormalizeWeights) =
        tryCreateInferenceSystem(alwaysNormalizeWeights)

    //member __.TryCreateInferenceSystem() =
    //    tryCreateInferenceSystem(false)

    member __.CreateInferenceSystem(alwaysNormalizeWeights) =
        match tryCreateInferenceSystem(alwaysNormalizeWeights) with
        | Ok is -> is
        | Error err -> failwith err

    //member __.CreateInferenceSystem() =
    //    __.CreateInferenceSystem(false)

    member __.TryAddInputVariable v = tryAddInputVariable v
                                      |> Option.toObj

    member __.AddInputVariable v = tryAddInputVariable v
                                   |> failOnError

    member __.TryAddInputVariables vs = tryAddInputVariables vs

    member __.AddInputVariables vs = tryAddInputVariables vs
                                     |> failOnErrorList



    member __.TryAddOutputVariable v = tryAddOutputVariable v
                                       |> Option.toObj

    member __.AddOutputVariable v = tryAddOutputVariable v
                                    |> failOnError

    member __.TryAddOutputVariables vs = tryAddOutputVariables vs

    member __.AddOutputVariables vs = tryAddOutputVariables vs
                                      |> failOnErrorList



    member __.TryAddRule s = tryAddRule s
                             |> Option.toObj

    member __.AddRule s = tryAddRule s
                          |> failOnError

    member __.TryAddRules ss = tryAddRules ss

    member __.AddRules ss = tryAddRules ss
                            |> failOnErrorList