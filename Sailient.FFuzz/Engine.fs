﻿module Sailient.FFuzz.Engine

open System
open System.Text
open Sailient.FSharp.Structures
open Sailient.FFuzz.Types
open Sailient.FFuzz.Operators
open Sailient.FFuzz.SetOperators



type Database =
    { InputVars  :FuzzyInputVar list 
      OutputVars :FuzzyOutputVar list }


module Database =

    let tryGetInputVar database candidate =
        database.InputVars
        |> List.tryFind (fun v -> v.Name = candidate) 

    let tryGetOutputVar database candidate =
        database.OutputVars
        |> List.tryFind (fun v -> v.Name = candidate) 


    let tryAddInputVar database (var :FuzzyInputVar) =
        if Option.isSome (tryGetInputVar database var.Name) then
            Result.Error <| sprintf "Variable %s already exists in inputs." var.Name
        elif Option.isSome (tryGetOutputVar database var.Name) then
            Result.Error <| sprintf "Variable %s already exists in outputs." var.Name
        else
            if Array.length var.Terms > 0 then
                Result.Ok <| { database with InputVars = database.InputVars @ [ var ] }
            else
                Result.Error <| sprintf "Variable %s has no terms." var.Name


    let tryAddOutputVar database (var :FuzzyOutputVar) =
        if Option.isSome (tryGetInputVar database var.Name) then
            Result.Error <| sprintf "Variable %s already exists in inputs." var.Name
        elif Option.isSome (tryGetOutputVar database var.Name) then
            Result.Error <| sprintf "Variable %s already exists in outputs." var.Name
        else
            if Array.length var.Terms > 0 then
                Result.Ok <| { database with OutputVars = database.OutputVars @ [ var ] }
            else
                Result.Error <| sprintf "Variable %s has no terms." var.Name


    let tryGetInputTerm database candidateVar candidateTerm =
        match tryGetInputVar database candidateVar with
        | None -> None
        | Some var ->
            match var.Terms |> Array.tryFind (fun t -> t.Name = candidateTerm) with
            | None -> None
            | Some term -> Some { term with Name = sprintf "%s_is_%s" var.Name term.Name }

    let tryGetOutputTerm database candidateVar candidateTerm =
        match tryGetOutputVar database candidateVar with
        | None -> None
        | Some var ->
            match var.Terms |> Array.tryFind (fun t -> t.Name = candidateTerm) with
            | None -> None
            | Some term -> Some { term with Name = sprintf "%s_is_%s" var.Name term.Name }



type internal FuzzyToken =
    | FuzzyTerm of FuzzyTerm
    | FuzzyModifier of UnaryOperator
    | FuzzySetModifier of UnarySetOperator
    | FuzzyUnaryOp of UnaryOperator
    | FuzzyBinaryOp of BinaryOperator


type internal FuzzyInputClause = FuzzyInputClause of FuzzyToken list

            
type internal Antecedent = Antecedent of FuzzyToken list

module internal Antecedent =

    let inputVars (Antecedent a) =
        let folder token l=
            match token with
            | FuzzyTerm t -> 
                if List.contains t.Var l then l
                else t.Var :: l
            | _ -> l
        List.foldBack folder a []

    let getFiringStrength world (Antecedent a) =
        let folder stack token =
            match token with
            | FuzzyTerm t ->
                Stack.push (FuzzyTerm.getFiringStrength world t) stack
            | FuzzyModifier m ->
                let v = Stack.top stack
                stack
                |> Stack.pop
                |> Stack.push (m.Func v)
            | FuzzyUnaryOp u ->
                let v = Stack.top stack
                stack
                |> Stack.pop
                |> Stack.push (u.Func v)
            | FuzzyBinaryOp b ->
                let v = Stack.top stack, Stack.peek 2 stack
                stack
                |> Stack.pop
                |> Stack.pop
                |> Stack.push (b.Func v)
            | FuzzySetModifier sm ->
                failwithf "Found a set modifier (%s) in clause listing (should never happen)." sm.Name

        let res = List.fold folder Stack.empty a
        if Stack.isEmpty res then
            failwith "Empty antecedent result (should never happen)."
        else
            Stack.top res


    let pprint (Antecedent l) =
        let folder acc x =
            match x with
            | FuzzyTerm x' -> acc + x'.Name + "\n"
            | FuzzyModifier x' -> acc + "  " + x'.Name + "\n"
            | FuzzySetModifier x' -> acc + "  " + x'.Name + "\n"
            | FuzzyUnaryOp x' -> acc + x'.Name + "\n"
            | FuzzyBinaryOp x' -> acc + x'.Name + "\n"
        let s = List.fold folder "" l
        s.Remove(s.Length - 1)



type internal Consequent =
    { Variable :string
      Term :string
      Weight :float }


module internal Consequent =


    let pprint (c :Consequent) =
        sprintf "%s_is_%s ( %g )" c.Variable c.Term c.Weight








type Rule =
    internal { index :int
               antecedent :Antecedent
               consequent :Consequent }

    member __.Index = __.index

module Rule =
    open System.Text

    let inputVars r = Antecedent.inputVars r.antecedent
    let outputVar r = r.consequent.Variable
    let outputTerm r = r.consequent.Term

    let weight r =  r.consequent.Weight

    let updateWeight r w =
        if w < 0.0 then
            Error "negative rule weight"
        else
            Ok { r with consequent = {r.consequent with Weight = w } }

    let getFiringStrength world rule =
        { 
          RuleFiringStrength.RuleIndex = Some <| rule.index
          Var = outputVar rule
          Term = outputTerm rule
          Strength = Antecedent.getFiringStrength world rule.antecedent
          Weight = weight rule
        }


    let pprint (r :Rule) =
        let iv = inputVars r
                 |> List.reduce (fun a b -> a + ", " + b)

        let sb = StringBuilder()
        ignore <| sb.AppendLine (sprintf "• RULE %i of (%s) -> %s" r.Index iv (outputVar r))
        ignore <| sb.AppendLine "--Antecedent--"
        ignore <| sb.AppendLine (Antecedent.pprint r.antecedent)
        ignore <| sb.AppendLine "--Consequent--"
        ignore <| sb.AppendLine (Consequent.pprint r.consequent)
        sb.ToString()



type RuleBase = RuleBase of Rule array

module RuleBase =

    let sort (RuleBase ruleArr) =
        RuleBase (Array.sortBy (fun r -> r.index) ruleArr)

    let addRule rb rule =
        let (RuleBase ruleArr) = sort rb
        let r = { rule with index = Array.length ruleArr }
        RuleBase (Array.append ruleArr [| r |])

    let addRules (rb :RuleBase) (rules :Rule seq) =
        Seq.fold addRule rb rules


    let count (RuleBase ruleArr) = Array.length ruleArr

    let selectByVar varName (RuleBase ruleArr) =
        Array.FindAll(ruleArr, fun r -> Rule.outputVar r = varName)
        |> Array.toSeq

    let selectByTerm varName termName (RuleBase ruleArr) =
        Array.FindAll(ruleArr, fun r -> (Rule.outputVar r , Rule.outputTerm r) = (varName, termName))
        |> Array.toSeq


    let private normalizeW (always :bool) (rules :Rule seq) =
        // assume all rules have the same output var & term
        let weights  = Seq.map Rule.weight rules
        let max = Seq.max weights
        if max = 0.0 then
            rules
        elif max <= 1.0 && not always then
            rules
        else
            rules
            |> Seq.map (fun (r :Rule) -> {r with consequent = { r.consequent with Weight = r.consequent.Weight / max }})
                


    let outputs (rb :RuleBase) =
        let (RuleBase ruleArr) = rb
        ruleArr
        |> Seq.map (fun (r :Rule) -> r.consequent.Variable, r.consequent.Term) 
        |> Seq.distinct

    let normalizeWeights (always :bool) (rb :RuleBase) =
        let outs = outputs rb
        let groupedByTerms = Seq.map (fun (v, t) -> selectByTerm v t rb) outs
        let norms = Seq.collect (normalizeW always) groupedByTerms 
        sort <| RuleBase (Seq.toArray norms)



    // weights

    let updateWeights (RuleBase ruleArr) weights always =
        let rulecount = Array.length ruleArr
        if rulecount <> Seq.length weights then
            Error <| sprintf "Expecting %i wieghts." rulecount
        else
            let newRulesR = Array.map2 Rule.updateWeight ruleArr (Seq.toArray weights)
                           |> Array.toList
                           |> Result.sequence
            match newRulesR with
            | Error err -> Error err
            | Ok newRules -> Ok <| normalizeWeights always (RuleBase (Seq.toArray newRules))


    let updateWeight (RuleBase ruleArr) (index :int, weight :float) =
        match (0 <= index && index < Array.length ruleArr), (0.0 <= weight && weight <= 1.0) with
        | false, _ -> Error "index is out of bounds"
        | _, false -> Error "Rule weight must be between 0 and 1"
        | true, true ->
            match Rule.updateWeight ruleArr.[index] weight with
            | Error err -> Error err
            | Ok r ->
                let newArr = Array.copy  ruleArr
                do Array.set newArr index r
                Ok <| (RuleBase newArr)


    let updateWeightsByIndex (rules :RuleBase) (indices :int seq, weights :float seq) =
        if (Seq.length indices <> Seq.length weights) then
            Error "You must supply an equal number of indices and weights"
        else
            let data = Seq.zip indices weights
            let folder acc x =
                match acc with
                | Ok rb -> (updateWeight rb x)
                | _ -> acc
            match Seq.fold folder (Ok rules) data with
            | Error err -> Error err
            | Ok rb' -> Ok rb'