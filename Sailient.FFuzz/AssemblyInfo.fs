﻿namespace Sailient.FFuzz.AssemblyInfo

open System.Reflection
open System.Runtime.CompilerServices
open System.Runtime.InteropServices

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[<assembly: AssemblyTitle("Sailient.FFuzz")>]
[<assembly: AssemblyDescription("Fuzzy logic library.")>]
[<assembly: AssemblyConfiguration("")>]
[<assembly: AssemblyCompany("Sailient")>]
[<assembly: AssemblyProduct("Sailient.FFuzz")>]
[<assembly: AssemblyCopyright("Copyright Frederic Becquelin 2021")>]
[<assembly: AssemblyTrademark("")>]
[<assembly: AssemblyCulture("")>]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[<assembly: ComVisible(false)>]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[<assembly: Guid("dfba20d2-0d47-47b9-bddf-9d1dc0ee2aae")>]

// Version information for an assembly consists of the following four values:
//
//       Major Version
//       Minor Version
//       Build Number
//       Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [<assembly: AssemblyVersion("1.0.*")>]
[<assembly: AssemblyVersion("1.0.2.0")>]
[<assembly: AssemblyFileVersion("1.0.2.0")>]

do
    ()