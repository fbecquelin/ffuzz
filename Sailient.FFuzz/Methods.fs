﻿module Sailient.FFuzz.MethodsConfig

open System
open System.Text

open Sailient.FFuzz.Operators
open Sailient.FFuzz.SetOperators

open FParsec

/// INFERENCE SYSTEM CONFIGURATION

type MethodsConfig =
    { 
      ComplementOperator    :UnaryOperator
      UnionOperator         :BinaryOperator
      IntersectionOperator  :BinaryOperator

      Modifiers             :UnaryOperator array
      SetModifiers          :UnarySetOperator array
      Sampling              :int
      
      Accumulator           :RuleAccumulation
      //Defuzzifier           :Defuzzifier 
    }

    override __.ToString() =

        let sb = StringBuilder()
        ignore <| sb.Append("_ComplementOperator: ")
        ignore <| sb.AppendLine(__.ComplementOperator.Name)
        ignore <| sb.Append("_UnionOperator: ")
        ignore <| sb.AppendLine(__.UnionOperator.Name)
        ignore <| sb.Append("_IntersectionOperator: ")
        ignore <| sb.AppendLine(__.IntersectionOperator.Name)

        ignore <| sb.Append("_Modifiers: ")
        let mods = 
            sprintf "%A" (Array.map (fun (m :UnaryOperator) -> m.Name) __.Modifiers)
            |> fun s -> s.Replace("\"", "")
            |> fun s -> s.Replace("[|", "")
            |> fun s -> s.Replace("|]", "")
        ignore <| sb.AppendLine(mods)

        ignore <| sb.Append("_SetModifiers: ")
        let smods = 
            sprintf "%A" (Array.map (fun (m :UnarySetOperator) -> m.Name) __.SetModifiers)
            |> fun s -> s.Replace("\"", "")
            |> fun s -> s.Replace("[|", "")
            |> fun s -> s.Replace("|]", "")
        ignore <| sb.AppendLine(smods)

        ignore <| sb.Append("_Sampling: ")
        ignore <| sb.AppendLine(string __.Sampling)

        ignore <| sb.Append("_Accumulator: ")
        ignore <| sb.AppendLine(__.Accumulator.Name)

        //ignore <| sb.Append("_Defuzzifier: ")
        //ignore <| sb.AppendLine(__.Defuzzifier.Name)
        sb.ToString()



 module MethodsConfig =
    open FParsec

    let makeDefault = 
        {
          ComplementOperator = UnaryOperator.OpStdNeg
          UnionOperator = BinaryOperator.OpMax
          IntersectionOperator  = BinaryOperator.OpMin
    
          Modifiers = [| |]
          SetModifiers = [| |]
          Sampling = 10 

          Accumulator = RuleAccumulation.Avg
          //Defuzzifier = Defuzzifier.CoG 
        }
          



type MethodsConfig with

    static member Minimal = MethodsConfig.makeDefault

    static member Default =
        { MethodsConfig.makeDefault with
            Modifiers = UnaryOperator.DefaultModifiers
            SetModifiers = [| UnarySetOperator.OpBelow ; UnarySetOperator.OpAbove ; UnarySetOperator.OpSlightly 10 |] }

    member __.SetComplementOperator s =
        match run OperatorParsing.pComplement s with
        | ParserResult.Success (op, _, _) -> { __ with ComplementOperator = op }
        | ParserResult.Failure (err, _, _) -> failwith err

    member __.SetComplementOperator op =
        { __ with ComplementOperator = op }



    member __.SetUnionOperator s =
        match run OperatorParsing.pUnion s with
        | ParserResult.Success (op, _, _) -> { __ with UnionOperator = op }
        | ParserResult.Failure (err, _, _) -> failwith err

    member __.SetUnionOperator op =
        { __ with UnionOperator = op }



    member __.SetIntersectionOperator s =
        match run OperatorParsing.pIntersection s with
        | ParserResult.Success (op, _, _) -> { __ with IntersectionOperator = op }
        | ParserResult.Failure (err, _, _) -> failwith err

    member __.SetIntersectionOperator op =
        { __ with IntersectionOperator = op }



    member __.SetRuleAccumulator s =
        match run OperatorParsing.pAccum s with
        | ParserResult.Success (op, _, _) -> { __ with Accumulator = op }
        | ParserResult.Failure (err, _, _) -> failwith err


    //member __.SetDefuzzifier s =
    //    match run OperatorParsing.pDefuzz s with
    //    | ParserResult.Success (op, _, _) -> { __ with Defuzzifier = op }
    //    | ParserResult.Failure (err, _, _) -> failwith err


    member __.SetSampling n =
        if n > 2 then
            { __ with Sampling = n }
        else
            failwith "Sampling must be at leat 3"


    member __.AddModifier (m :UnaryOperator) =
        let names = Array.map (fun (m :UnaryOperator) -> m.Name) __.Modifiers
        if Array.contains m.Name names then
            failwithf "Modifier '%s' already exists." m.Name
        else    
            { __ with Modifiers = Array.append __.Modifiers [| m |] }

    member __.AddModifiers ms =
        let folder (acc :MethodsConfig) m = acc.AddModifier m
        Seq.fold folder __ ms

    member __.AddSetModifier (sm :UnarySetOperator) =
        { __ with SetModifiers = Array.append __.SetModifiers [| sm |] }

    member __.AddSetModifiers sms =
        let folder (acc :MethodsConfig) sm = acc.AddSetModifier sm
        Seq.fold folder __ sms