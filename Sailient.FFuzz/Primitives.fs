﻿module Sailient.FFuzz.Primitives

open System
open FParsec


[<Measure>]
type truth

[<Measure>]
type real


/// Name validation
module internal Identifier =
    type T(name :string) =
        let value = name
        member this.Value = value

    let (|Identifier|) x = x

    let create (candidate :string) =
        let pId :Parser<string, unit> = 
                let label = "Identifier.
                             \n Identifiers must start with an uppper case ascii letter, then ascii letters, digits or underscore.
                             \n e.g. : Temp2"
                let pAllowedChars = choice [ asciiLetter ; digit ; pchar '_' ]
                (asciiUpper .>>. many1Chars pAllowedChars .>> eof |>> (fun (h, t) -> string h + t)) <?> label
        match run pId (candidate.Trim()) with
        | ParserResult.Success (name, _, _) -> Result.Ok    <| T name
        | ParserResult.Failure (_, err, _)  -> Result.Error <| err.ToString()



/// Point in a membership function.
type FuzzyPoint =
    struct
        val X :float<real>
        val Y :float<truth>
        new(x, (y :float<truth>)) = { X = x; Y = max 0.0<truth> (min y 1.0<truth>) }
        override __.ToString() =
            sprintf "(%f;%f)" __.X __.Y
    end

module FuzzyPoint =
    let inline getRealPart (p :FuzzyPoint) = p.X
    let inline getTruthPart (p :FuzzyPoint) = p.Y

    let private isValidFloat x = not (Double.IsNaN x || Double.IsInfinity x)

    let create x y =
        if not (isValidFloat x || isValidFloat y) then 
            Result.Error "NaN or Infinity value"
        elif (y < 0.0 || y > 1.0) then
            Result.Error "Y is not within [0 - 1]"
        else
            Result.Ok (FuzzyPoint(x * 1.0<real>, y * 1.0<truth>))

    let createTrue x = FuzzyPoint(x, 1.0<truth>)
    let createFalse x = FuzzyPoint(x, 0.0<truth>)

    let isTrue (p :FuzzyPoint) =
        p.Y = 1.0<truth>

    let isFalse (p :FuzzyPoint) =
        p.Y = 0.0<truth>


/// Result of Area calculation.
type AreaProperties = { Area :float ; Centroid :FuzzyPoint }


/// Pair of points in a membership function.
type FuzzySegment =
    struct
        val Start :FuzzyPoint
        val End :FuzzyPoint
        new((p1 : FuzzyPoint), (p2 : FuzzyPoint)) =
            if (p1.X <= p2.X) then
                    { Start = p1; End = p2 }
            else
                    { Start = p2; End = p1}
    end

module FuzzySegment =
    open FuzzyPoint

    let isValid (s :FuzzySegment) =
        s.Start.X < s.End.X
        && 0.0<truth> <= s.Start.Y && s.Start.Y <= 1.0<truth>
        && 0.0<truth> <= s.End.Y && s.End.Y <= 1.0<truth>

    let isIncreasing (s :FuzzySegment) =
        s.Start.Y < s.End.Y

    let isDecreasing (s :FuzzySegment) =
        s.Start.Y > s.End.Y

    let isFlat (s :FuzzySegment) =
        s.Start.Y = s.End.Y

    let isTrue (s :FuzzySegment) =
        isTrue s.Start && isTrue s.End 

    let isFalse (s :FuzzySegment) =
        isFalse s.Start && isFalse s.End


    let midPoint (s :FuzzySegment) =
        FuzzyPoint(0.5 * (s.Start.X + s.End.X), 0.5 * (s.Start.Y + s.End.Y))

    let maxPoint (s :FuzzySegment) =
        if s.Start.Y > s.End.Y then
            s.Start
        elif s.Start.Y < s.End.Y then
            s.End
        else
            midPoint s

    let minPoint (s :FuzzySegment) =
        if s.Start.Y > s.End.Y then
            s.End
        elif s.Start.Y < s.End.Y then
            s.Start
        else
            midPoint s

    //let area (s :FuzzySegment) =
    //    let b = ( s.End.X - s.Start.X)
    //    let h = 0.5 * (s.Start.Y + s.End.Y)
    //    b * h / 1.0<real truth>

    //let centroid (s :FuzzySegment) =
    //    let d = (s.Start.Y + s.End.Y)
    //    if d <> 0.0<truth> then
    //        let x = (s.Start.X * s.Start.Y + s.End.X * s.End.Y) / d
    //        let y = 0.5 * d
    //        FuzzyPoint(x, y)
    //    else
    //        let x = 0.5 * (s.Start.X + s.End.X)
    //        FuzzyPoint(x, d)
     

    let areaProps (s :FuzzySegment) =
        let areaProperties (sb, (xb, yb), st, (xt, yt)) =
            let weightedAverage(xs :float<'u> list) (ws :float<'w> list) :float<'u> =
                let folder (accX, accW) (x, w) =
                    accX + x * w, accW + w
                let totX, totW = Seq.fold folder (0.0<_>, 0.0<_>) (Seq.zip xs ws)
                if totW = 0.0<_> then 0.0<_> else totX / totW
            let x = weightedAverage [ xb ; xt ] [ sb ; st ]
            let y = weightedAverage [ yb ; yt ] [ sb ; st ]
            { AreaProperties.Area = (sb + st) / 1.0<real truth> ; Centroid = FuzzyPoint(x, y) }
        let l = s.End.X - s.Start.X
        let b = min s.Start.Y s.End.Y
        let h = (max s.Start.Y s.End.Y) - b
        let sb = l * b
        let st = 0.5 * l * h
        let xb, yb = s.Start.X + 0.5 * l, 0.5 * b
        if isFlat s then
            { AreaProperties.Area = sb / 1.0<real truth>; Centroid = FuzzyPoint(xb, yb) }
        else
            let tempX = (s.Start.X + s.End.X) / 3.0
            let tempY = (s.Start.Y + s.End.Y) / 3.0
            if isIncreasing s then
                let xt = tempX + s.End.X / 3.0
                let yt = tempY + s.End.Y / 3.0
                areaProperties (sb, (xb, yb), st, (xt, yt))
            else
                let xt = tempX + s.Start.X / 3.0
                let yt = tempY + s.Start.Y / 3.0
                areaProperties (sb, (xb, yb), st, (xt, yt))


    let tryEval (x :float<real>) (s :FuzzySegment)=
        if x = s.Start.X then 
            Some s.Start.Y
        elif x = s.End.X then 
            Some s.End.Y
        elif x < s.Start.X then 
            None
        elif x > s.End.X then 
            None
        else
            let m = (s.End.Y - s.Start.Y) / (s.End.X - s.Start.X)
            Some <| (s.Start.Y + (x - s.Start.X) * m)


    let divide step (s :FuzzySegment) =
        let range (a: float<real>) (b: float<real>) n =
            let l = b - a
            let r = [ for i in 1..n -> (a + (float i) * l / (float n)) ]
            r

        let n = ceil >> int <| (s.End.X - s.Start.X) / step
        if n < 2 || isFlat s then
            [ s.End ]
        else
            let r = range s.Start.X s.End.X n
            [ for x in r do
                       match tryEval x s with
                       | None -> yield None
                       | (Some y) -> yield Some <| FuzzyPoint(x, y) ]
            |> List.choose id
        

    let tryXCal (y :float<truth>) (s :FuzzySegment) =
        let y1 = s.Start.Y
        let y2 = s.End.Y
        let minT, maxT = min y1 y2 , max y1 y2

        if y < minT || maxT < y then            // above or below y
            None
        elif isFlat s && s.Start.Y = y then     // co-linear
            Some s.Start.X
        else                                    // intersection
            let m =  (s.End.Y - s.Start.Y) / (s.End.X - s.Start.X)
            Some <| (s.Start.X + (y - s.Start.Y) / m)


    let crop (y :float<truth>) (s :FuzzySegment) =
        if y < (minPoint s).Y then   // segment above y, and flat
            [ FuzzyPoint(s.End.X, y) ]
        elif (maxPoint s).Y <= y then // segment below y
             [ s.End ]
        else
            let xO = tryXCal y s              // not flat, compute intersection
            match xO with
            | None -> failwith "No intersection found"     // segment below y
            | Some x ->                       // intersection
                if isIncreasing s then
                    [ s.Start ; FuzzyPoint(x, y) ]
                else
                    [ FuzzyPoint(x, y) ; s.End ]

    /// (m, c, min, max) such that Y = mX + c with X in [min, max]
    let cartesianRep (seg :FuzzySegment) = 
        let m = (seg.End.Y - seg.Start.Y) / (seg.End.X - seg.Start.X)
        let c = seg.Start.Y - seg.Start.X * m
        m, c, (seg.Start.X, seg.End.X)

    let getIntersectionPoint (seg1 :FuzzySegment) (seg2 :FuzzySegment) =
        let m1, c1, (min1, max1) = cartesianRep seg1
        let m2, c2, (min2, max2) = cartesianRep seg2

        if (max2 <= min1) || (max1 <= min2) then
            // no overlap
            []
        else
            match m1 = m2, c1 = c2 with
            | false, _ -> // intersect
                let x = (c1 - c2) / (m2 - m1)
                let y = m1 * x + c1
                [ FuzzyPoint(x, y) ]
            | true, true -> // colinear
                let x1 = max min1 min2
                let y1 = m1 * x1 + c1
                let x2 = min max1 max2
                let y2 = m1 * x2 + c1
                [ FuzzyPoint(x1, y1) ; FuzzyPoint(x2, y2) ]
            | true, false -> // parallel
                []


/// Indicates if a piecewise membership function is an edge.
type Edge =
    | No    = 0
    | Left  = 1
    | Right = 2
    | Both  = 3
         


/// Membership function
type PiecewiseSet =
    private { points :FuzzyPoint array 
              edge :Edge }
    member __.Points = __.points
    member __.Edge = __.edge

module PiecewiseSet =
    open System.ComponentModel.Design

    let create (edge :Edge) (points :FuzzyPoint array) =
        let pts = points |> Array.sortBy FuzzyPoint.getRealPart
        { PiecewiseSet.points = pts ; edge = edge }

    let createSet (points :FuzzyPoint array) =
        match (Array.head points).Y > 0.0<truth>, (Array.last points).Y > 0.0<truth> with
        | true, true ->  create Edge.Both points
        | true, false ->  create Edge.Left points
        | false, true ->  create Edge.Right points
        | false, false ->  create Edge.No points


    let createLeftEdge x1 x2 =
        if x1 < x2 then
            let pts = [| FuzzyPoint(x1 * 1.0<real>, 1.0<truth>)
                         FuzzyPoint(x2 * 1.0<real>, 0.0<truth>) |]
            Result.Ok { PiecewiseSet.points = pts ; edge = Edge.Left }
        else
            Result.Error "Values are not sorted."

    let createRightEdge x1 x2 =
        if x1 < x2 then
            let pts = [| FuzzyPoint(x1 * 1.0<real>, 0.0<truth>)
                         FuzzyPoint(x2 * 1.0<real>, 1.0<truth>) |]
            Result.Ok { PiecewiseSet.points = pts ; edge = Edge.Right }
        else
            Result.Error "Values are not sorted."



    let bounds (fs :PiecewiseSet) =
        let p0 = fs.Points |> Array.head
        let p1 = fs.Points |> Array.last
        p0.X, p1.X
        

    let segments (fs :PiecewiseSet) =
        let len = Array.length fs.Points
        [ for i in 0..(len - 2) do yield FuzzySegment(fs.Points.[i], fs.Points.[i + 1]) ]


    let slopedSegmentsLength (fs :PiecewiseSet) =
        let f s = 
            if FuzzySegment.isFlat s then
                s.End.X - s.Start.X
            else
                0.0<real>
        fs
        |> segments
        |> List.map f
        |> List.sum

    let divide (sampling :int) (fs :PiecewiseSet) =
        let step = 0.5 * (slopedSegmentsLength fs) / (float (max sampling 1))
        let list = fs.Points.[0] :: List.collect (FuzzySegment.divide step) (segments fs)
        create fs.Edge (List.toArray list)


    /// Extends an edge membership function towards the variable bounds.
    let extend (pws :PiecewiseSet) bounds =
        let xMin, xMax = bounds
        match pws.Edge with
        | Edge.Left -> 
            if xMin < pws.Points.[0].X then
                let pts = Array.append [| FuzzyPoint(xMin, 1.0<truth>) |] pws.Points
                Result.Ok <| create Edge.Left pts
            elif xMin = pws.Points.[0].X then
                Result.Ok <| create Edge.Left pws.Points
            else
                Result.Error "Edge set is outside bounds"
        | Edge.Right ->
            if xMax > (Array.last pws.Points).X then
                let pts = Array.append pws.Points [| FuzzyPoint(xMax, 1.0<truth>) |] 
                Result.Ok <| create Edge.Right pts
            elif xMax = (Array.last pws.Points).X then
                Result.Ok <| create Edge.Right pws.Points
            else
                Result.Error "Edge set is outside bounds"
        | _ -> 
            Result.Ok <| pws


    /// Remove un-necessary points in a membership function.
    let simplify (pws :PiecewiseSet) =
        let areColinear (s1, s2) =
            let m1, _, _ = FuzzySegment.cartesianRep s1
            let m2, _, _ = FuzzySegment.cartesianRep s2
            m1 = m2
        
        let folder acc ((s1, s2) :FuzzySegment * FuzzySegment) =
            let prevSeg = FuzzySegment (List.head acc, s1.Start)
            match areColinear(prevSeg, s1), areColinear(s1, s2) with
            | true, true ->
                let t = List.tail acc
                s2.End::t
            | true, false ->
                let t = List.tail acc
                s2.End::s1.End::t
            | false, true ->
                s2.End::acc
            | false, false ->
                s2.End::s1.End::acc

        pws
        |> segments
        |> List.pairwise 
        |> List.fold folder [ pws.Points.[0] ]
        |> List.distinct
        |> List.rev
        |> List.toArray
        |> create pws.Edge


    let eval fs (x :float<real>)  =
        let minX, maxX = bounds fs
        if x <= minX then 
            fs.Points 
            |> Array.head 
            |> FuzzyPoint.getTruthPart
        elif maxX <= x then 
            fs.Points 
            |> Array.last 
            |> FuzzyPoint.getTruthPart
        else
            try
                fs
                |> segments 
                |> List.pick (FuzzySegment.tryEval x)
            with
            | _ -> 0.0<truth>


    let truncate (pws :PiecewiseSet) (y :float<truth>) =
        let folder acc seg =
            if (FuzzySegment.minPoint seg).Y > y then
                FuzzyPoint(seg.End.X, y)::acc
            else if (FuzzySegment.maxPoint seg).Y <= y then
                seg.End::acc
            else
                match FuzzySegment.tryXCal y seg with
                | Some x ->                      
                    if FuzzySegment.isIncreasing seg then
                        seg.Start::FuzzyPoint(x, y)::acc
                    else if FuzzySegment.isDecreasing seg then
                        FuzzyPoint(x, y)::seg.End::acc
                    else
                        failwith "Internal error: truncate - flat segment should've been handled above."
                | None -> failwith "Internal error: truncate - flat segment should've been handled above."

        let fstPoint = Array.head pws.Points
        let lstPoint = Array.last pws.Points
        let init = [ FuzzyPoint(fstPoint.X, min y fstPoint.Y) ]
        let pts = pws
                  |> segments
                  |> List.fold folder init
                  
        FuzzyPoint(lstPoint.X, min y lstPoint.Y)::pts
        |> List.toArray
        |> create pws.Edge
        |> simplify


    let getIntersectionPoints (pws1 :PiecewiseSet) (pws2 :PiecewiseSet) =
        let f pws s =
            segments pws
            |> List.collect (FuzzySegment.getIntersectionPoint s)
        segments pws1
        |> List.collect (f pws2)


    let getAllXs (pws1 :PiecewiseSet) (pws2 :PiecewiseSet) =
        [ for p in pws1.Points do yield p.X]
         @ [ for p in (getIntersectionPoints pws1 pws2) do yield p.X ]
         @ [ for p in pws2.Points do yield p.X ]
        |> List.distinct
        |> List.sort

    let private doSetBinaryOp op (pws1 :PiecewiseSet) (pws2 :PiecewiseSet) =
        let xs = getAllXs pws1 pws2
        let ys = [ for x in xs do yield op (eval pws1 x) (eval pws2 x) ]
        if List.length xs <> List.length ys then
            failwith "evaluation failed"
        else
            List.zip xs ys
            |> List.map (fun (a, b) -> FuzzyPoint(a, b))
            |> List.toArray


    let intersection (pws1 :PiecewiseSet) (pws2 :PiecewiseSet) = 
        let edge = if pws1.Edge = pws2.Edge then pws1.Edge
                   else Edge.No
        let pts = doSetBinaryOp min pws1 pws2
        create edge pts
        |> simplify

    let union (pws1 :PiecewiseSet) (pws2 :PiecewiseSet) = 
        let edge =  pws1.Edge + pws2.Edge
        let pts = doSetBinaryOp max pws1 pws2
        create edge pts
        |> simplify


    let areaProps fs :AreaProperties =
         let folder (accA,accX) s =
             let props = FuzzySegment.areaProps s
             accA + props.Area , accX + props.Centroid.X * props.Area
         let totA, totX = List.fold folder (0.0, 0.0<real>) (segments fs)
         if totA <> 0.0 then
             { AreaProperties.Area = totA ; Centroid = FuzzyPoint(totX / totA , 0.0<truth>) }
         else
             { AreaProperties.Area = 0.0 ; Centroid = FuzzyPoint(0.0<real>, 0.0<truth>) }


    let getMaximums (fs :PiecewiseSet) =
        let folder (maxY, maxArr) (point :FuzzyPoint) =
            if point.Y < maxY then
                maxY, maxArr
            elif maxY < point.Y  then
                point.Y, [| point |]
            else // equal
                maxY, Array.append maxArr [| point |]

        let init = (0.0<truth>, [| fs.Points.[0] |])
        let _, arr = Array.fold folder init fs.Points
        arr

    let getMaximum fs =
        let folder (accX, accY, accW) (p :FuzzyPoint) =
            accX + p.X , accY + p.Y, accW + 1.0
        let ps = getMaximums fs
        let init = (0.0<real>, 0.0<truth>, 0.0)
        let totX, totY, totW = Array.fold folder init ps
        let x = totX / totW
        let y = totY / totW
        FuzzyPoint(x, y)


    let getLeftMaximum = getMaximums >> Array.head

    let getRightMaximum = getMaximums >> Array.last


type PiecewiseSet with
        member __.Bounds = PiecewiseSet.bounds __
        member __.Segments = List.toArray (PiecewiseSet.segments __)
        member __.Eval(x) = PiecewiseSet.eval __ x
        member __.Truncate(y) = PiecewiseSet.truncate __ y
        member __.AreaProps = PiecewiseSet.areaProps __



/// Singleton membership function.
type Singleton = 
    { Point :FuzzyPoint}

module Singleton =
    let create x =
        {Singleton.Point = FuzzyPoint(x, 1.0<truth>) }

    let eval fs (x :float<real>) =
        if x = fs.Point.X then 1.0<truth> else 0.0<truth>

    let truncate fs act = 
        { fs with Point = FuzzyPoint(fs.Point.X, act) }




type FuzzySet =
    | PiecewiseSet of PiecewiseSet
    | Singleton of Singleton

module FuzzySet =
    let points fs =
        match fs with
        | PiecewiseSet pws -> pws.Points
        | Singleton s      -> [| s.Point |]

    let bounds fs =
        match fs with
        | PiecewiseSet pws -> PiecewiseSet.bounds pws
        | Singleton s      -> (s.Point.X ,s.Point.X)

    let intersection fs1 fs2 =
        match fs1, fs2 with
        | PiecewiseSet pws1, PiecewiseSet pws2 -> PiecewiseSet <| PiecewiseSet.intersection pws1 pws2
        | _ -> failwith "Cannot compute intersection with singleton sets."

    let union fs1 fs2 =
        match fs1, fs2 with
        | PiecewiseSet pws1, PiecewiseSet pws2 -> PiecewiseSet <| PiecewiseSet.union pws1 pws2
        | _ -> failwith "Cannot compute intersection with singleton sets."

    let truncate fs act =
        match fs with
        | PiecewiseSet pws -> PiecewiseSet <| PiecewiseSet.truncate pws act
        | Singleton s      -> Singleton <| Singleton.truncate s act

    let areaProps (fs:FuzzySet) =
        match fs with
        | PiecewiseSet pws -> PiecewiseSet.areaProps pws
        | Singleton s      -> { AreaProperties.Area = 0.0 ; Centroid = s.Point }

    let getMaximum (fs:FuzzySet) =
        match fs with
        | PiecewiseSet pws -> PiecewiseSet.getMaximum pws
        | Singleton s      -> s.Point

    let getLeftMaximum (fs:FuzzySet) =
        match fs with
        | PiecewiseSet pws -> PiecewiseSet.getLeftMaximum pws
        | Singleton s      -> s.Point

    let getRightMaximum (fs:FuzzySet) =
        match fs with
        | PiecewiseSet pws -> PiecewiseSet.getRightMaximum pws
        | Singleton s      -> s.Point


type FuzzySet with
    member __.Points = FuzzySet.points __
    member __.Truncate(a) = FuzzySet.truncate __ a
