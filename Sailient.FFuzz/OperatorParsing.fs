﻿module Sailient.FFuzz.OperatorParsing

open Sailient.FFuzz.Operators
open FParsec

// empty base class for boxing in C#
type OperatorParser () = class end


let pUnaryOpList (unaryOps :UnaryOperator array) :Parser<UnaryOperator,_> =
    unaryOps
    |> Seq.sortByDescending (fun x -> x.Name.Length)
    |> Seq.map (fun o -> pstring o.Name >>% o) 
    |> choice

let pBinaryOpList (binaryOps :BinaryOperator array) :Parser<BinaryOperator,_> =
    binaryOps
    |> Seq.sortByDescending (fun x -> x.Name.Length)
    |> Seq.map (fun o -> pstring o.Name >>% o) 
    |> choice

let pAccumList (accums :RuleAccumulation array) :Parser<RuleAccumulation,_> =
    accums
    |> Seq.sortByDescending (fun x -> x.Name.Length)
    |> Seq.map (fun o -> pstring o.Name >>% o) 
    |> choice

let pDefuzzList (defuzzifiers :Defuzzifier array) :Parser<Defuzzifier,_> =
    defuzzifiers
    |> Seq.sortByDescending (fun x -> x.Name.Length)
    |> Seq.map (fun o -> pstring o.Name >>% o) 
    |> choice




let pComplement :Parser<UnaryOperator,unit> = pUnaryOpList UnaryOperator.DefaultComplements

type ComplementParser(?complements) =
    inherit OperatorParser()

    let m_parser = defaultArg complements UnaryOperator.DefaultComplements
                   |> pUnaryOpList

    member __.Parse s = match run m_parser s with
                        | ParserResult.Success (s,_,_) -> s
                        | ParserResult.Failure (err,_,_) -> failwith err
    member __.Default = UnaryOperator.OpStdNeg




let pUnion :Parser<BinaryOperator,unit> = pBinaryOpList BinaryOperator.DefaultUnions

type UnionParser(?unions) =
    inherit OperatorParser()

    let m_parser = defaultArg unions BinaryOperator.DefaultUnions
                   |> pBinaryOpList

    member __.Parse s = match run m_parser s with
                        | ParserResult.Success (s,_,_) -> s
                        | ParserResult.Failure (err,_,_) -> failwith err
    member __.Default = BinaryOperator.OpMax




let pIntersection :Parser<BinaryOperator,unit> = pBinaryOpList BinaryOperator.DefaultIntersections

type IntersectionParser(?intersections) =
    inherit OperatorParser()

    let m_parser = defaultArg intersections BinaryOperator.DefaultIntersections
                   |> pBinaryOpList

    member __.Parse s = match run m_parser s with
                        | ParserResult.Success (s,_,_) -> s
                        | ParserResult.Failure (err,_,_) -> failwith err
    member __.Default = BinaryOperator.OpMin




let pAccum :Parser<RuleAccumulation,unit> = pAccumList RuleAccumulation.DefaultRuleAccumulations

type AccumParser(?accumulators) =
    inherit OperatorParser()

    let m_pAccum = defaultArg accumulators RuleAccumulation.DefaultRuleAccumulations
                   |> pAccumList
    
    member __.Parse s = match run m_pAccum s with
                        | ParserResult.Success (s,_,_) -> s
                        | ParserResult.Failure (err,_,_) -> failwith err
    member __.Default = RuleAccumulation.Max





let pDefuzz :Parser<Defuzzifier,unit> = pDefuzzList Defuzzifier.DefaultDefuzzifiers

type DefuzzParser(?defuzzifiers) =
    inherit OperatorParser()

    let m_pDefuzz = defaultArg defuzzifiers Defuzzifier.DefaultDefuzzifiers
                    |> pDefuzzList

    member __.Parse s = match run m_pDefuzz s with
                        | ParserResult.Success (s,_,_) -> s
                        | ParserResult.Failure (err,_,_) -> failwith err
    member __.Default = Defuzzifier.CoG

