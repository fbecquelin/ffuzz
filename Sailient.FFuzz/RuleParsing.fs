﻿module Sailient.FFuzz.RuleParsing

open FParsec
open Sailient.FSharp.Structures
open Sailient.FFuzz.ParsingPrimitives
open Sailient.FFuzz.Operators
open Sailient.FFuzz.SetOperators
open Sailient.FFuzz.Types
open Sailient.FFuzz.MethodsConfig
open Sailient.FFuzz.Engine


type private InputClauseClauseParser(methods, db) =
    let m_methods = methods
    let m_inputVars = db.InputVars

    // methods
    let pComplement :Parser<FuzzyToken,unit>=
            strCI_s1 "NOT" 
            >>% FuzzyModifier { UnaryOperator.Name = "NOT" ; Func = m_methods.ComplementOperator.Func }

    let pMod :Parser<FuzzyToken,unit> =
        let pm (m :UnaryOperator) = strCI_s m.Name >>% FuzzyModifier m
        choice (Seq.map pm m_methods.Modifiers)

    let pSMod :Parser<FuzzyToken,unit> =
        let pm (m :UnarySetOperator) =  strCI_s m.Name >>% FuzzySetModifier m
        choice (Seq.map pm m_methods.SetModifiers)

    let pModifier = pMod <|> pSMod

    let pModifiers = (tuple2 pModifier (opt pModifier))
                     |>> fun (m1, m2) ->
                                match m1, m2 with
                                // just one modifier
                                | m1' , None -> [ m1' ]

                                // two modifiers:
                                // - first one is an set modifier => compose both as a set modifier
                                | FuzzySetModifier m1' , Some (FuzzyModifier m2') ->
                                      [ FuzzySetModifier <| UnarySetOperator.compose (UnarySetOperator.map m2' m_methods.Sampling) m1']

                                // - second one is an set modifier => do not compose
                                | FuzzyModifier m1' , Some (FuzzySetModifier m2') ->
                                      [ FuzzyModifier m1' ; FuzzySetModifier m2' ]

                                // - both are set modifiers => compose both as a set modifier
                                | FuzzySetModifier m1' , Some (FuzzySetModifier m2') ->
                                    [ FuzzySetModifier <| UnarySetOperator.compose m2' m1' ]

                                // - both are modifiers => compose both as a modifier
                                | FuzzyModifier m1' , Some (FuzzyModifier m2') ->
                                    [ FuzzyModifier <| UnaryOperator.compose m2' m1' ]

                                | _, _ -> []


    let pc (v :string) (terms :string array) =
        let pv = str_s1 v <?> "Input Variable"
        let pt :Parser<string,unit> = choice (Seq.map (fun t -> str_ws t) terms)

        pv .>> strCI_s1 "IS" .>>. opt pComplement .>>. opt pModifiers .>>. pt .>> spaces
        |>> fun (((varN, compl), mods), termN) ->
                 let resO = Database.tryGetInputTerm db varN termN
                 match resO with
                 | None -> failwith (sprintf "(%s_is_%s) was not found in the database (should never happen)." varN termN)
                 | Some ft ->
                     let revListing = [ FuzzyTerm ft ]
                     match mods, compl with
                     | None, None ->
                         revListing
                     | None, Some compl' ->
                          compl' :: revListing
                     | Some mods', None ->
                         mods' @ revListing
                     | Some mods', Some compl' ->
                          compl' ::  mods' @ revListing

    let processClause (listing :FuzzyToken list) =
        let folder token stack =
            match token with
            | FuzzyTerm t -> Stack.push token stack
            | FuzzySetModifier m ->
                match Stack.top stack with
                | FuzzyTerm t ->
                    let s = Stack.pop stack
                    Stack.push (FuzzyTerm (FuzzyTerm.transformTerm t m)) s 
                | _ -> Stack.push token stack
            | FuzzyModifier m ->
                match Stack.top stack with
                | FuzzyModifier m' ->
                    let s = Stack.pop stack
                    Stack.push (FuzzyModifier (UnaryOperator.compose m' m)) s 
                | _ -> Stack.push token stack
            | x -> failwithf "Unrecognized operation: (%O) in clause listing (should never happen)." x

        List.foldBack folder listing Stack.empty
        |> Stack.rev
        |> fun l -> FuzzyInputClause l 

    let pClause :Parser<FuzzyInputClause,unit>=
        let varDicts = m_inputVars
                       |> List.map (fun v -> v.Name, v.TermsNames)
        choice (Seq.map (fun (v,ts) -> pc v ts ) varDicts)
        |>> processClause


    member __.Parser = pClause

    member __.Parse s =
        match run pClause s with
        | ParserResult.Success (c, _, _) -> c
        | ParserResult.Failure (e, _, _) -> failwith e




type private AntecedentParser(methods, db) =
    let m_methods = methods
    let m_ClauseParser = InputClauseClauseParser(m_methods, db)

    let opp = new OperatorPrecedenceParser<_,_,_>()
    let expr :Parser<_,_> = opp.ExpressionParser

    let term = (m_ClauseParser.Parser .>> spaces |>> fun (FuzzyInputClause c) -> c )
                <|> between (str_ws "(") (str_ws ")") expr

    do
        opp.TermParser <- term 

        let f_not l =
            l @ [ FuzzyUnaryOp m_methods.ComplementOperator ]
        let f_and a b =
            a @ b @ [ FuzzyBinaryOp m_methods.IntersectionOperator ]
        let f_or a b =
            a @ b @ [ FuzzyBinaryOp m_methods.UnionOperator ]

        opp.AddOperator(PrefixOperator("NOT", spaces1, 3, true, f_not))
        opp.AddOperator(PrefixOperator("not", spaces1, 3, true, f_not))

        opp.AddOperator(InfixOperator("AND", spaces1, 2, Associativity.Left, f_and))
        opp.AddOperator(InfixOperator("and", spaces1, 2, Associativity.Left, f_and))

        opp.AddOperator(InfixOperator("OR", spaces1, 1, Associativity.Left,  f_or)) 
        opp.AddOperator(InfixOperator("or", spaces1, 1, Associativity.Left,  f_or)) 
   
    let pAnte = (expr .>> spaces) |>> fun (l) -> Antecedent l

    member __.Parser = pAnte





type private ConsequentParser(db) =
    let m_outputVars = db.OutputVars


    let pw = str_s "*" >>. pfloat .>> spaces
             |>> fun x ->
                if x < 0.0 then failwith "Negative rule weight."
                else x

    // let pw = str_s "*" >>. pfloat .>> (manyChars (pchar ' ') >>. skipChar ';' .>> spaces <|> eof)

    let processConseq  ((varN, termN), weight) =
        match Database.tryGetOutputTerm db varN termN, weight with
        | Some ft , Some w -> { Consequent.Variable = varN ; Term = termN ; Weight = w }
        | Some ft , None   -> { Consequent.Variable = varN ; Term = termN ; Weight = 1.0 }
        | _ -> failwith "nothing…"

    let pc (v :string) (terms :string array) =
        let pv = str_s1 v <?> "Output Variable"
        let pt :Parser<string,unit> = choice (Seq.map (fun t -> str_ws t) terms)

        pv .>> strCI_s1 "IS" .>>. pt .>>. opt pw .>> skipRestOfLine false
        |>> processConseq


    let pConseq :Parser<_,_> =
        let varDicts = m_outputVars
                       |> List.map (fun v -> v.Name, v.TermsNames)
        choice (Seq.map (fun (v,ts) -> pc v ts ) varDicts)

    member __.Parser = pConseq
    



type RuleParser(methods, db) =
    let m_AnteParser = AntecedentParser(methods, db)
    let m_ConseqParser = ConsequentParser(db)

    let prule = 
        strCI_ws1 "IF" >>. m_AnteParser.Parser .>>. (strCI_ws1 "THEN" >>. m_ConseqParser.Parser) .>> spaces
        

    member __.Parse (i, s) =
        match run prule s with
        | ParserResult.Success ((a,c), _, _) -> Result.Ok { Rule.index = i ; antecedent = a ; consequent = c }
        | ParserResult.Failure (e, _, _) -> Result.Error e

