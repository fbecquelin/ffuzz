﻿module Sailient.FFuzz.SetOperators

// Operators that act on entire membership functions

open System
open Sailient.FFuzz.Primitives
//open Sailient.FFuzz.Types
open Sailient.FFuzz.Operators


type UnarySetOperator =
       { Name :string
         Func :(FuzzySet -> FuzzySet) }

        override __.ToString() =
             sprintf "FFuzz.UnarySetOperator %s" __.Name

module UnarySetOperator =

    let apply (m :UnaryOperator) sampling (fs :FuzzySet) =
        match fs with
        | PiecewiseSet fs ->
            let pts = fs
                      |> PiecewiseSet.divide sampling 
                      |> fun fs -> fs.Points
                      |> Array.map (UnaryOperator.apply m) 
            PiecewiseSet <| PiecewiseSet.create fs.Edge pts
        | Singleton s -> 
            Singleton { s with Point = UnaryOperator.apply m s.Point }


    let map (m :UnaryOperator) sampling =
        { UnarySetOperator.Name = m.Name  ; Func = apply m sampling }


    let compose (a :UnarySetOperator) (b :UnarySetOperator) =
        { Name = sprintf "%s_%s" b.Name a.Name // PN
          Func = a.Func >> b.Func }



    let createCustom (name :string) (func :Func<FuzzyPoint[],FuzzyPoint[]>)  :UnarySetOperator =

        let castResult pts =
            if Array.length pts = 1 then
                Singleton <|{ Singleton.Point = Array.head pts }
            else if Array.length pts > 1 then
                PiecewiseSet <| PiecewiseSet.createSet pts
            else
                failwith "Empty set."

        let unarySetOpDelegate fs =
             match fs with
             | PiecewiseSet pws ->
                 func.Invoke(pws.Points)
                 |> castResult
             | Singleton s -> 
                 func.Invoke( [| s.Point |] )
                 |> castResult

        match Identifier.create name with
        | Ok ident ->
            { UnarySetOperator.Name = ident.Value ; Func = unarySetOpDelegate }
        | Error err -> failwith err


    // Operators | Modifiers

    let norm (fs :FuzzySet) =
        match fs with
        | PiecewiseSet pws ->
            let maxY = pws.Points
                       //|> Array.map (fun p -> p.Y / 1.0<truth>)
                       |> Array.map FuzzyPoint.getTruthPart
                       |> Array.max
                       |> float
            let pts = pws.Points
                      |> Array.map (fun p -> FuzzyPoint(p.X, p.Y / maxY))
            PiecewiseSet <| PiecewiseSet.create pws.Edge pts
        | Singleton s ->
            Singleton { s with Point = FuzzyPoint(s.Point.X, 1.0<truth>) }



    // Modifiers

    let belowSet (pws :PiecewiseSet) =
        let lm = PiecewiseSet.getLeftMaximum pws
        let f (p :FuzzyPoint) =
            if p.X <= lm.X then
               FuzzyPoint(p.X, 1.0<truth> - p.Y)
            else 
               FuzzyPoint(p.X, 0.0<truth>)
        let result = Array.map f pws.Points
        PiecewiseSet.create Edge.Left result
        |> PiecewiseSet.simplify

    let belowLeftEdge (pws :PiecewiseSet) =
        let fsR = PiecewiseSet.createLeftEdge 
                    ((2.0 * pws.Points.[0].X - pws.Points.[1].X) / 1.0<real>) 
                    (pws.Points.[0].X / 1.0<real>)
        match fsR with
        | Ok s -> s
        | Error err -> pws

    let belowSingleton (s :Singleton) =
         let fsR = PiecewiseSet.createLeftEdge
                        (s.Point.X / 1.0<real> - Double.Epsilon)
                        (s.Point.X / 1.0<real>)
         match fsR with
         | Ok fs -> fs
         | Error err -> failwith "Error applying BELOW to singleton set"

    let belowPiecewise (pws :PiecewiseSet) =
        match pws.Edge with
        | Edge.Left | Edge.Both -> belowLeftEdge pws
        | _ -> belowSet pws

    let below fs =
        match fs with
        | PiecewiseSet pws -> PiecewiseSet <| belowPiecewise pws
        | Singleton s -> PiecewiseSet <| belowSingleton s


    let aboveSet (pws :PiecewiseSet) =
        let rm = PiecewiseSet.getRightMaximum pws
        let f (p :FuzzyPoint) =
            if rm.X <= p.X then
               FuzzyPoint(p.X, 1.0<truth> - p.Y)
            else 
               FuzzyPoint(p.X, 0.0<truth>)
        let result = Array.map f pws.Points
        PiecewiseSet.create Edge.Right result
        |> PiecewiseSet.simplify

    let aboveRightEdge (fs :PiecewiseSet) =
        let fsR = PiecewiseSet.createRightEdge
                    (fs.Points.[1].X / 1.0<real>) 
                    ((2.0 * fs.Points.[1].X - fs.Points.[0].X) / 1.0<real>)
        match fsR with
        | Ok s -> s
        | Error err -> fs

    let aboveSingleton (s :Singleton) =
        let fsR = PiecewiseSet.createLeftEdge
                        (s.Point.X / 1.0<real>)
                        (s.Point.X / 1.0<real> + Double.Epsilon)
        match fsR with
        | Ok fs -> fs
        | Error err -> failwith "Error applying BELOW to singleton set"

    let abovePiecewise (pws :PiecewiseSet) =
        match pws.Edge with
        | Edge.Right | Edge.Both -> aboveRightEdge pws
        | _ -> aboveSet pws

    let above fs =
        match fs with
        | PiecewiseSet pws -> PiecewiseSet <| abovePiecewise pws
        | Singleton s -> PiecewiseSet <| aboveSingleton s


    // SLIGHTLY X = intensify ( norm (plus X AND not very X) )
    let slightly n fs =
        match fs with
        | PiecewiseSet pws ->
            let ratherA = (map UnaryOperator.OpRather n).Func fs
            let notVery = (map (UnaryOperator.compose UnaryOperator.OpStdNeg UnaryOperator.OpVery) n).Func fs
            FuzzySet.intersection ratherA notVery
            |> norm
            |> (map UnaryOperator.OpIntensify n).Func
        | Singleton _ -> failwithf "Cannot apply SLIGHTLY modifier to a singleton set."

    
type UnarySetOperator with
    static member OpNorm  = { UnarySetOperator.Name = "NORM"  ; Func = UnarySetOperator.norm  }

    static member OpBelow = { UnarySetOperator.Name = "BELOW" ; Func = UnarySetOperator.below }
    static member OpAbove = { UnarySetOperator.Name = "ABOVE" ; Func = UnarySetOperator.above }
    static member OpSlightly n = { UnarySetOperator.Name = "SLIGHTLY" ; Func = UnarySetOperator.slightly n }


    /// <summary>Create a custom unary set operator with a delegate.</summary>
    /// <param name="name">The name of the operator.</param>
    /// <param name="func">The delegate function of type <c>Func<FuzzyPoint[],FuzzyPoint[]></c>.</param>
    /// <returns>The unary set operator.</returns>
    /// <example>
    /// You may branch betwwen singletons and piecewise membership functions in the delegate.
    /// <code>
    /// var opFunc =
    ///    (pArr =>
    ///    { // Your code here…
    ///        if (pArr.Length == 1) // Singleton
    ///        {
    ///          var p = pArr[0];
    ///          return new FFuzz.Primitives.FuzzyPoint[] { new FFuzz.Primitives.FuzzyPoint(p.X, 1.0) };
    ///        }
    ///        else if (pArr.Length > 1) // Piecewise
    ///        {
    ///          var maxY = pArr
    ///            .Select(p => p.Y)
    ///            .Max();
    ///          if (maxY == 0) maxY = 1;
    ///          var newArr = pArr
    ///            .Select(p => new FFuzz.Primitives.FuzzyPoint(p.X, p.Y / maxY))
    ///            .ToArray();
    ///          return newArr;
    ///        }
    ///        else
    ///        {
    ///          throw new Exception("Empty array! this should never happen. Please contact the developer…");
    ///        }
    ///    });
    ///
    /// var myOp = FFuzz.SetOperators.UnarySetOperator.CreateCustomUnarySetOperator("NORM", opFunc);
    /// </code>
    /// </example>
    static member CreateCustomUnarySetOperator(name, func) =
         UnarySetOperator.createCustom name func



type BinarySetOperator =
       { Name :string
         Func :FuzzySet * FuzzySet -> FuzzySet }

        override __.ToString() =
             sprintf "FFuzz.BinarySetOperator %s" __.Name

module BinarySetOperator =

    let doSetBinaryOp (op :BinaryOperator) ((fs1 :FuzzySet), (fs2 :FuzzySet)) =
        match fs1, fs2 with
        | PiecewiseSet pws1, PiecewiseSet pws2 ->
            let xs = PiecewiseSet.getAllXs pws1 pws2
            let ys = [ for x in xs do yield op.Func (PiecewiseSet.eval pws1 x, PiecewiseSet.eval pws2 x) ]
            List.zip xs ys
            |> List.map (fun (a, b) -> FuzzyPoint(a, b))
            |> List.toArray
            |> PiecewiseSet.create Edge.No
            |> PiecewiseSet
        | _ -> failwithf "Cannot apply binary set operator %s to singleton sets" op.Name

    let map (op :BinaryOperator) =
        { BinarySetOperator.Name = op.Name 
          Func = doSetBinaryOp op  }

type BinarySetOperator with
    static member OpBetween = 
        let f (fs1, fs2) =
            FuzzySet.intersection (UnarySetOperator.above fs1) (UnarySetOperator.below fs2)
        { BinarySetOperator.Name = "BETWEEN" ; Func = f }


