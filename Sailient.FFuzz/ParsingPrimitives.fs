﻿module Sailient.FFuzz.ParsingPrimitives 

open FParsec

let str s = pstring s
let strCI s = pstringCI s

let str_ws s = pstring s .>> spaces
let strCI_ws s = pstringCI s .>> spaces
let str_s s = pstring s .>> skipMany (pchar ' ')
let strCI_s s = pstringCI s .>> skipMany (pchar ' ')

let str_ws1 s = spaces >>. pstring s .>> spaces1
let strCI_ws1 s = spaces >>. pstringCI s .>> spaces1
let str_s1 s = pstring s .>> many1Chars (pchar ' ')
let strCI_s1 s = pstringCI s .>> many1Chars (pchar ' ')

let str_ws2 s = spaces1 >>. pstring s .>> spaces1
let strCI_ws2 s = spaces1 >>. pstringCI s .>> spaces1
let str_s2 s =  many1Chars (pchar ' ') >>. pstring s .>>  many1Chars (pchar ' ')
let strCI_s2 s =  many1Chars (pchar ' ') >>. pstringCI s .>>  many1Chars (pchar ' ')

// Applies popen, then pchar repeatedly until pclose succeeds, returns the string in the middle
let manyCharsBetween popen pclose pchar :Parser<_,_> =
        popen >>? many1CharsTill pchar pclose

// Parses any string between popen and pclose
let anyStringBetween popen pclose :Parser<_,_> =
        manyCharsBetween popen pclose anyChar

let manyCharsBefore pclose pchar :Parser<_,_> = many1CharsTill pchar pclose

let anyStringBefore pclose :Parser<_,_> = manyCharsBefore pclose anyChar


let pBlank  :Parser<char,unit> = choice [pchar ' ' ; pchar '\t' ]

let pBlanks  :Parser<_,_> = many pBlank
let pBlanks1  :Parser<_,_> = many1 pBlank

let skipBlank  :Parser<_,_> = skipMany pBlank
let skip1Blank  :Parser<_,_> = skipMany1 pBlank

