﻿module Sailient.FSharp.Structures.Stack

exception EmptyStack of string
exception OutOfRange of string

type T<'a> = T of 'a list


let empty = T []

let ofList l = T l
let ofArray a = T <| List.ofArray a
let ofSeq s = T <| List.ofSeq s

let items (s :T<'a>) :'a list = match s with T l -> l

let isEmpty (s :T<'a>) = (items s = [])

let depth (s :T<'a>) = items s |> List.length

let push (x :'a) (s :T<'a>) = T(x::items s)

let pop (s :T<'a>) =
    match items s with
    | [] -> raise(EmptyStack "Cannot pop from an empty stack.")
    | _::tail -> T(tail)

let top (s :T<'a>) =
    match items s with
    | [] -> raise(EmptyStack "Cannot get top of an empty stack.")
    | head::_ -> head

let private doPeek f (n :int) (s :T<'a>) =
     match items s with
     | [] -> raise(EmptyStack "Cannot get top of an empty stack.")
     | l -> let dpth = depth s
            match n with
            | n when n < 1 -> raise(OutOfRange ("n must be positive."))
            | n when dpth < n  -> raise(OutOfRange (sprintf "n is larger then the depth (n=%i, depth= %i)." n dpth)) 
            | n -> f n l

let peek n = doPeek (fun n -> List.item (n-1)) n

let peekL n = doPeek (fun n -> List.take n) n


let rev (s :T<'a>) = List.rev (items s)

let map (f :'a -> 'b) (s :T<'a>) = List.map f (items s)
let iter (f :'a -> unit) (s :T<'a>) = List.iter f (items s)
